<?php
ini_set('display_errors', '1');
require 'flight/Flight.php';
require 'policy.php';

Flight::path(__DIR__ . '/stack/');
Flight::path(__DIR__ . '/stack/api');
Flight::path(__DIR__ . '/stack/interfaces');
Flight::path(__DIR__ . '/stack/middleware');
Flight::path(__DIR__ . '/stack/repository');
Flight::path(__DIR__ . '/stack/services');
Flight::path(__DIR__ . '/stack/utility');
Flight::path(__DIR__ . '/stack/utility/auth');
Flight::path(__DIR__ . '/stack/utility/db');
Flight::path(__DIR__ . '/stack/utility/jwt');

date_default_timezone_set('America/Toronto');
date_default_timezone_set('EST');

$config = Configuration::getConfig();
Flight::set('config', $config);
Flight::set('swagger', json_decode(file_get_contents($config['swagger_json']), true));
Flight::set('policy', $policies);
Flight::set('middlewareMapping', $middlewareMapping);
Flight::set('db', Configuration::selectDB($config));

require 'stack/api/client.php';
// require 'stack/api/operation.php';
require 'stack/api/adminOperation.php';
// require 'stack/api/app.php';
// require 'stack/api/organization.php';
// require 'stack/api/schema.php';
// require 'stack/api/user.php';
Flight::start();

?>