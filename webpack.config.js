module.exports = {
    entry: "./clientAdmin/app/entry.js",
    output: {
        path: __dirname,
        filename: "./clientAdmin/bundle.js"
    },
    module: {
        loaders: [
        { test: /\.html$/, loaders: ["html"] }
        ]
    }
};