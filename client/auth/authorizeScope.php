<?php 
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title><?php echo $logo;?></title>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <style>
        body {
            background: #E9E9E9 url('http://static01.linkedin.com/scds/common/u/img/bg/bg_grain_200x200.png');
            font-family: Arial, Helvetica, "Nimbus Sans L", sans-serif;
            margin: 0px;
        }
        
        a:visited {
            color: #069;
        }
        
        a {
            text-decoration: none;
            color: #069;
            outline: none;
        }
        
        #content ul li {
            font-style: italic;
            font-size: 14px;
            color: #D85700;
        }
        
        .authorize_options {
            margin: 0;
            padding: 0;
        }
        
        #content .authorize_options li {
            list-style-type: none;
            font-style: normal;
            display: inline-block;
        }
        
        .authorize_options li.cancel {
            margin-left: 15px;
            padding-top: 8px;
        }
        
        #container {
            z-index: 10001;
            margin-top: 40px;
        }
        
        #container {
            margin-left: auto;
            margin-right: auto;
            max-width: 974px;
            width: 100%;
            padding: 0;
        }
        
        #content {
            margin-left: auto;
            margin-right: auto;
            /*width: 974px;*/
            background: white;
            padding: 10px 20px;
            -moz-box-shadow: 0 0 1px #999;
            -webkit-box-shadow: 0 0 1px #999;
            -o-box-shadow: 0 0 1px #999;
            box-shadow: 0 0 1px #999;
            margin-bottom: 10px;
        }
        
        .global-nav {
            z-index: 10002;
            margin-bottom: 0px;
            left: 0;
            position: fixed;
            top: 0;
            width: 100%;
        }
        
        .global-nav .top-nav {
            height: 38px;
            margin: 0;
            filter: progid: DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#FF696969', endColorstr='#FF252525');
            background: #252525;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, dimGray), color-stop(95%, #363636), color-stop(97%, #313131), color-stop(100%, #252525));
            background-image: -webkit-linear-gradient(top, dimGray 0%, #363636 95%, #313131 97%, #252525 100%);
            background-image: -moz-linear-gradient(top, dimGray 0%, #363636 95%, #313131 97%, #252525 100%);
            background-image: -o-linear-gradient(top, dimGray 0%, #363636 95%, #313131 97%, #252525 100%);
            background-image: -ms-linear-gradient(top, dimGray 0%, #363636 95%, #313131 97%, #252525 100%);
            background-image: linear-gradient(top, dimGray 0%, #363636 95%, #313131 97%, #252525 100%);
        }
        
        .global-nav .wrapper {
            margin: 0 auto;
            width: 974px;
            z-index: 10004;
        }
        
        .primary,
        .wrapper {
            clear: both;
        }
        
        .global-nav .util {
            float: right;
        }
        
        .global-nav ul,
        .global-nav li,
        .global-nav p,
        .global-nav fieldset,
        #section-header ul,
        #section-header li,
        #section-header p,
        #section-header,
        .global-search fieldset,
        .global-search select,
        .global-search input,
        #footer ul,
        #footer li,
        #footer p {
            margin: 0;
            padding: 0;
            list-style-type: none;
            border: 0;
            outline: 0;
            background: none;
            vertical-align: middle;
        }
        
        .wrapper .util .tab.username-cont {
            z-index: 10010;
        }
        
        .global-nav .util .jump {
            position: absolute;
            left: -9999px;
        }
        
        .global-nav .util .tab {
            line-height: 38px;
        }
        
        .global-nav .tab {
            float: left;
            _border: none;
            _clear: none;
            _padding: 0;
            position: relative;
            z-index: 10010;
        }
        
        .global-nav p,
        .global-nav li {
            margin-bottom: 0;
        }
        
        .global-nav ul,
        .global-nav li,
        .global-nav p,
        .global-nav fieldset,
        #section-header ul,
        #section-header li,
        #section-header p,
        #section-header,
        .global-search fieldset,
        .global-search select,
        .global-search input,
        #footer ul,
        #footer li,
        #footer p {
            margin: 0;
            padding: 0;
            list-style-type: none;
            border: 0;
            outline: 0;
            background: none;
            vertical-align: middle;
        }
        
        .global-nav .util .username {
            color: white;
        }
        
        .global-nav .tab .tab-name {
            color: #71C5EF;
            display: block;
            padding: 0 10px;
        }
        
        .global-nav .util .username .menu-indicator {
            margin: 0 0 0 4px;
            display: -moz-inline-box;
            -moz-box-orient: vertical;
            display: inline-block;
            vertical-align: middle;
            height: 0;
            width: 0;
            _font-size: 0;
            _line-height: 0;
            border-style: dashed;
            border-color: transparent;
            border-width: 4px 4px 0;
            border-top-color: white;
            border-top-style: solid;
        }
        
        .global-nav .tab .tab-name span {
            position: relative;
            z-index: 10011;
        }
        
        .global-nav .logo {
            float: left;
            margin: 8px 10px 0 3px;
        }
        
        .global-nav .logo a {
            color: white;
        }
        
        .global-nav .logo a span {
            background-color: #069;
            padding: 0px 3px;
        }
        
        .global-nav .account {
            float: left;
            font-size: 12px;
            margin-top: 14px;
        }
        
        .global-nav #header-notifications.v2 {
            background: url('http://static01.linkedin.com/scds/common/u/img/anim/anim_loading_16x16.gif') no-repeat -12345px -12345px;
        }
        
        .global-nav .header-notifications {
            border: none;
            float: left;
            margin-left: 15px;
            padding: 0;
        }
        
        .global-nav .bottom-nav {
            height: 39px;
            margin: 0;
            filter: progid: DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#FF121212', endColorstr='#FF303030');
            background: #303030;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #121212), color-stop(100%, #303030));
            background-image: -webkit-linear-gradient(top, #121212 0%, #303030 100%);
            background-image: -moz-linear-gradient(top, #121212 0%, #303030 100%);
            background-image: -o-linear-gradient(top, #121212 0%, #303030 100%);
            background-image: -ms-linear-gradient(top, #121212 0%, #303030 100%);
            background-image: linear-gradient(top, #121212 0%, #303030 100%);
        }
        
        
        .global-nav .nav .tab {
            line-height: 37px;
        }
        
        .global-nav .tab:hover .tab-name,
        .global-nav .tab.hover .tab-name,
        .global-nav .tab.selected .tab-name {
            color: white;
        }
        
        .global-nav .tab .tab-name {
            color: #71C5EF;
            display: block;
            padding: 0 10px;
            text-decoration: none;
        }
        
        .global-nav {
            font-family: sans-serif;
        }
        
        .global-nav {
            font-size: 13px;
        }
        
        .global-nav .account {
            float: left;
            font-size: 12px;
            margin-top: 14px;
        }
        
        .global-nav .account span {
            color: #87877D;
        }
        .button {
            color: #333;
            text-shadow: 0 1px 0 white;
            border: 1px solid #D4D4D4;
            border-bottom-color: #BCBCBC;
            background: #FAFAFA;
            background: -moz-linear-gradient(#FAFAFA, #EAEAEA);
            background: -webkit-linear-gradient(#FAFAFA, #EAEAEA);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='#fafafa', endColorstr='#eaeaea')";
            position: relative;
            display: inline-block;
            padding: 0 10px 0 10px;
            font-family: Helvetica, arial, freesans, clean, sans-serif;
            font-size: 13px;
            font-weight: bold;
            line-height: 24px;
            white-space: nowrap;
            border-radius: 3px;
            cursor: pointer;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            text-decoration: none;
        }

        .button:hover {
            color: white;
            text-decoration: none;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.3);
            border-color: #518CC6;
            border-bottom-color: #2A65A0;
            background: #599BDC;
            background: -moz-linear-gradient(#599BDC, #3072B3);
            background: -webkit-linear-gradient(#599BDC, #3072B3);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bdc', endColorstr='#3072b3')";
        }
@media only screen and (min-width: 401px) {
    #container {
        width: 80%;
    }
    #content {
        width: 90%;
    }
}                
@media only screen and (max-width: 400px) {
    #container {
        width: 320px;
        min-width: 320px;
    }
    #content {
        width: 280px;
    }
}        

    </style>
</head>

<body>
    <div id="header" class="global-nav member">
        <div class="top-nav">
            <div class="wrapper">
                <h2 class="logo"><a href="#" title="Home"><?php echo $logo;?></a></h2>

                <div class="account">
                    <span></span>
                </div>
            </div>
        </div>

    </div>
    <div id="container">
            <div id="content" role="main">
                <h3><?php echo $appName?> would like to access some of your information:</h3>
                <ul>
                    <?php foreach($scope as $row) {
                        echo "<li>$row</li>";
                    }?>
                </ul>
                <p>Click the button below to allow access.
                    <ul class="authorize_options">
                        <li>
                            <form action="/oauth2/lockdin/authorize?client_id=demoapp&amp;nonce=1521426086&amp;redirect_uri=http%3A%2F%2Fbrentertainment.com%2Foauth2%2Fclient%2Freceive_authcode&amp;response_type=code&amp;scope=openid&amp;state=b6960d7f941d366c0bc53a20e753e237" method="post">
                                <input type="submit" class="button authorize" value="Yes, I Authorize This Request" />
                                <input type="hidden" name="authorize" value="1" />
                            </form>
                        </li>
                        <li class="cancel">
                            <form id="cancel" action="/oauth2/lockdin/authorize?client_id=demoapp&amp;nonce=1521426086&amp;redirect_uri=http%3A%2F%2Fbrentertainment.com%2Foauth2%2Fclient%2Freceive_authcode&amp;response_type=code&amp;scope=openid&amp;state=b6960d7f941d366c0bc53a20e753e237" method="post">
                                <a href="#" onclick="document.getElementById('cancel').submit()">cancel</a>
                                <input type="hidden" name="authorize" value="0" />
                            </form>
                        </li>
                        <li style="clear:both"></li>
                    </ul>
            </div>
    </div>
</body>

</html>