<?php 

function validationServiceTest () {
    $swagger = json_decode(swaggerTestValue(), true);
    $v1 = new ValidationService($swagger);

    assert('$v1->invokeMethod($v1, "_checkRequired", [["a","b"], ["b","a"]]) == true', 'required matching');
    assert('$v1->invokeMethod($v1, "_checkRequired",[["a","b"], ["b","a"]]) == true', 'required matching');
    assert('$v1->invokeMethod($v1, "_checkRequired",[[], []]) == true', 'required matching empty');
    assert('$v1->invokeMethod($v1, "_checkRequired",[[], ["a"]]) == true', 'required not matching');
    assert('$v1->invokeMethod($v1, "_checkRequired",[["b"], ["a"]]) == false', 'required not matching empty');
    
    $type1 = [
        'a' => [
            'type' => 'string'
        ]
    ];

    $type2 = [
        'a' => [
            'type' => 'integer'
        ]
    ];
      
    $type3 = [
        'a' => [
            'type' => 'number'
        ]
    ];
      
    $type4 = [
        'a' => [
            'type' => 'boolean'
        ]
    ];
                
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type1, ["a" => "a"]]) == true', 'matching string types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type1, ["a" => 1]]) == false', 'fail matching string types');

    assert('$v1->invokeMethod($v1, "_checkTypes", [$type2, ["a" => 2]]) == true', 'matching integer types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type2, ["a" => "2"]]) == false', 'fail matching integer types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type2, ["a" => "a"]]) == false', 'fail matching integer types');

    assert('$v1->invokeMethod($v1, "_checkTypes", [$type3, ["a" => 2.43]]) == true', 'matching number types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type3, ["a" => "2.34"]]) == true', 'fail matching number types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type3, ["a" => "a"]]) == false', 'fail matching number types');
            
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type4, ["a" => true]]) == true', 'matching boolean types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type4, ["a" => false]]) == true', 'matching boolean types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type4, ["a" => "true"]]) == false', 'fail matching boolean types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type4, ["a" => "a"]]) == false', 'fail matching boolean types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type4, ["a" => "1"]]) == false', 'fail matching boolean types');
    assert('$v1->invokeMethod($v1, "_checkTypes", [$type4, ["a" => "2"]]) == false', 'fail matching boolean types');
    
    $enumType1 = [
        'a' => [
            'type' => 'string',
            'enum' => ['a','b']
        ]
    ];

    $enumType2 = [
        'a' => [
            'type' => 'integer',
            'enum' => [1,2,3,4]
        ]
    ];
      
    $enumType3 = [
        'a' => [
            'type' => 'number',
            'enum' => [2.3,4.5]
        ]
    ];
      
    $enumType4 = [
        'a' => [
            'type' => 'boolean',
            'enum' => [true]
        ]
    ];

    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType1, ["a" => "a"]]) == true', 'matching string enum');
    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType1, ["a" => "c"]]) == false', 'fail matching string enum');

    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType2, ["a" => 2]]) == true', 'matching integer enum');
    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType2, ["a" => 5]]) == false', 'fail matching integer enum');

    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType3, ["a" => 2.3]]) == true', 'matching number enum');
    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType3, ["a" => 5.4]]) == false', 'fail matching number enum');
            
    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType4, ["a" => true]]) == true', 'matching boolean enum');
    assert('$v1->invokeMethod($v1, "_checkEnum", [$enumType4, ["a" => false]]) == false', 'matching boolean enum');
    
    $queryType1 = [
    "name" => "response_code",
    "in" => "query",
    "description" => "Response Code",
    "type" => "string",
    "required" => true
    ];

    $queryType2 = [
    "name" => "response_code",
    "in" => "query",
    "description" => "Response Code",
    "type" => "integer",
    "required" => true
    ];   
    
    $queryType3 = [
    "name" => "response_code",
    "in" => "query",
    "description" => "Response Code",
    "type" => "integer",
    "required" => false
    ];     
    
    $queryType4 = [
    "name" => "response_code",
    "in" => "query",
    "description" => "Response Code",
    "type" => "number",
    "required" => true
    ];           
    
    $queryType5 = [
    "name" => "response_code",
    "in" => "query",
    "description" => "Response Code",
    "type" => "boolean",
    "required" => true
    ];           
    
    assert('$v1->invokeMethod($v1, "_validateQuery", [$queryType1, ["response_code" => "a"]]) == false', 'validating query parameter');

    assert('$v1->invokeMethod($v1, "_validateQuery", [$queryType1, ["response_code" => 1]]) == APIResponses::TYPEERROR', 'validating query parameter type error');    

    assert('$v1->invokeMethod($v1, "_validateQuery", [$queryType1, []]) == APIResponses::REQUIREERROR', 'validating query parameter type error');

    assert('$v1->invokeMethod($v1, "_validateQuery", [$queryType2, ["response_code" => 1]]) == false', 'validating query parameter integer type');    
    
    assert('$v1->invokeMethod($v1, "_validateQuery", [$queryType3, ["response_code" => 1]]) == false', 'validating query parameter integer type that not required');
    
    assert('$v1->invokeMethod($v1, "_validateQuery", [$queryType4, ["response_code" => 1.5]]) == false', 'validating query parameter integer type'); 
    
    assert('$v1->invokeMethod($v1, "_validateQuery", [$queryType5, ["response_code" => false]]) == false', 'validating query parameter integer type');     
    
    
    $pathType1 = [
    "name" => "response_code",
    "in" => "path",
    "description" => "Response Code",
    "type" => "string",
    "required" => true
    ];    
    
    $pathType2 = [
    "name" => "response_code",
    "in" => "path",
    "description" => "Response Code",
    "type" => "number",
    "required" => true
    ];    
    
    $pathType3 = [
    "name" => "response_code",
    "in" => "path",
    "description" => "Response Code",
    "type" => "integer",
    "required" => true
    ];    
    
    assert('$v1->invokeMethod($v1, "_validatePath", [$pathType1, ["response_code" => "sds"]]) == false', 'validating path parameter string type');
    
    assert('$v1->invokeMethod($v1, "_validatePath", [$pathType2, ["response_code" => 1.5]]) == false', 'validating path parameter number type');
    
    assert('$v1->invokeMethod($v1, "_validatePath", [$pathType3, ["response_code" => 1]]) == false', 'validating path parameter integer type');             
    
    $v1TestInput = [
        "response_type" => "code",
        "redirect_uri" => "string"
    ];
    assert('$v1->validate("/authorize", "post", $v1TestInput) == APIResponses::REQUIREERROR', 'required field failed');
    $v2TestInput = [
        "response_type" => 1,
        "client_id" => "string",
        "redirect_uri" => "string"
    ];    
    assert('$v1->validate("/authorize", "post", $v2TestInput) == APIResponses::TYPEERROR', 'field wrong type');
    $v3TestInput = [
        "response_type" => 'not valid response type',
        "client_id" => "string",
        "redirect_uri" => "string"
    ];       
    assert('$v1->validate("/authorize", "post", $v3TestInput) == APIResponses::ENUMERROR', 'field wrong enum value');
}

function swaggerTestValue () {
    return '{"swagger":"2.0","info":{"description":"This is the API Swagger for Manaknight Backend as a Service","version":"1.0.0","title":"Manaknight API Swagger","termsOfService":"http://swagger.io/terms/","contact":{"email":"support@manaknight.com"}},"produces":["application/json"],"securityDefinitions":{"api_auth":{"type":"apiKey","name":"api_key","in":"header"},"authorization_auth":{"type":"authorization","name":"authorization","in":"header"}},"host":"localhost:8888/baas","basePath":"/v1","tags":[{"name":"Organization","description":"Organization CRUD"},{"name":"App","description":"Application CRUD"},{"name":"Admin Operations","description":"Admin Operations"},{"name":"Member Operations","description":"Member Operations"}],"schemes":["http"],"paths":{"/organization":{"get":{"summary":"Get all Organizations","operationId":"getOrganizations","tags":["Organization"],"description":"Returns the list of Organizations","parameters":[],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/organization"},"examples":{"application/json":[{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization":"organization #1","createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}]}},"default":{"description":"Invalid request.","schema":{"$ref":"#/definitions/Error"}}}},"post":{"tags":["Organization"],"summary":"Add a new Organization","description":"Add a new Organization to Manaknight BaaS","operationId":"addOrganization","consumes":["application/json"],"parameters":[{"in":"body","name":"body","description":"Organization Body that needs to be stored","required":true,"schema":{"$ref":"#/definitions/organization"}}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/organization"},"examples":{"application/json":{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization":"organization #1","createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}}},"405":{"description":"Invalid input"}}}},"/organization/{organization_id}":{"get":{"operationId":"getOrganization","summary":"Get an Organization","tags":["Organization"],"description":"Returns a Organizations","parameters":[{"name":"organization_id","in":"path","description":"Organization Id","type":"string","required":true}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/organization"},"examples":{"application/json":{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization":"organization #1","createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}}},"default":{"description":"Invalid request.","schema":{"$ref":"#/definitions/Error"}}}},"put":{"tags":["Organization"],"summary":"Update an existing Organization","description":"Update an existing Organizations name or status","operationId":"updateOrganization","consumes":["application/json"],"parameters":[{"name":"organization_id","in":"path","description":"Organization Id","type":"string","required":true},{"in":"body","name":"body","description":"Organization Body that needs to be stored","required":true,"schema":{"$ref":"#/definitions/organization"}}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/organization"},"examples":{"application/json":{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization":"organization #1","createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}}},"400":{"description":"Invalid ID supplied"},"404":{"description":"Organization not found"},"405":{"description":"Validation exception"}}}},"/organization/{organization_id}/app":{"get":{"tags":["App"],"summary":"Get all Application in organization","description":"Get all Application in organization","operationId":"getApplications","consumes":["application/json"],"parameters":[{"name":"organization_id","in":"path","description":"Organization Id","type":"string","required":true}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/application"},"examples":{"application/json":[{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization_id":"61a19196-5f87-4242-a1e0-96e81315904b","admin_user_id":"71a19196-5f87-4242-a1e0-96e81315904b","detail":{},"createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}]}},"404":{"description":"Organization not found"}}},"post":{"tags":["App"],"summary":"Create a new Application","description":"Create a new Application","operationId":"addApplication","consumes":["application/json"],"parameters":[{"name":"organization_id","in":"path","description":"Organization Id","type":"string","required":true},{"in":"body","name":"body","description":"App Body that needs to be stored","required":true,"schema":{"$ref":"#/definitions/application"}}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/application"},"examples":{"application/json":{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization_id":"61a19196-5f87-4242-a1e0-96e81315904b","admin_user_id":"71a19196-5f87-4242-a1e0-96e81315904b","detail":{},"createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}}},"400":{"description":"Invalid ID supplied"},"404":{"description":"Organization not found"},"405":{"description":"Validation exception"}}}},"/organization/{organization_id}/app/{app_id}":{"get":{"operationId":"getApp","summary":"Get an App","tags":["App"],"description":"Returns an App","parameters":[{"name":"organization_id","in":"path","description":"Organization Id","type":"string","required":true},{"name":"app_id","in":"path","description":"App Id","type":"string","required":true}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/application"},"examples":{"application/json":{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization_id":"61a19196-5f87-4242-a1e0-96e81315904b","admin_user_id":"71a19196-5f87-4242-a1e0-96e81315904b","detail":{},"createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}}},"default":{"description":"Invalid request.","schema":{"$ref":"#/definitions/Error"}}}},"put":{"tags":["App"],"summary":"Update an existing App","description":"Update an existing App","operationId":"updateApp","consumes":["application/json"],"parameters":[{"name":"organization_id","in":"path","description":"Organization Id","type":"string","required":true},{"name":"app_id","in":"path","description":"Application Id","type":"string","required":true},{"in":"body","name":"body","description":"App Body that needs to be stored","required":true,"schema":{"$ref":"#/definitions/application"}}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/application"},"examples":{"application/json":{"id":"81a19196-5f87-4242-a1e0-96e81315904b","organization_id":"61a19196-5f87-4242-a1e0-96e81315904b","admin_user_id":"71a19196-5f87-4242-a1e0-96e81315904b","detail":{},"createDate":"2016-01-01","modifiedDate":"2016-01-01 12:01","status":1}}},"400":{"description":"Invalid ID supplied"},"404":{"description":"Organization not found"},"405":{"description":"Validation exception"}}},"delete":{"operationId":"deleteApplication","summary":"Delete an App","tags":["App"],"description":"Delete an App","parameters":[{"name":"organization_id","in":"path","description":"Organization Id","type":"string","required":true},{"name":"app_id","in":"path","description":"App Id","type":"string","required":true}],"responses":{"200":{"description":"Successful request.","schema":{"$ref":"#/definitions/Error"},"examples":{"application/json":{"message":"deleted","status":"success"}}},"default":{"description":"Invalid request.","schema":{"$ref":"#/definitions/Error"}}}}},"/authorize":{"post":{"operationId":"authorize","summary":"Authorize","tags":["Authorization"],"description":"Authorization Oauth page","parameters":[{"in":"body","name":"body","description":"Oauth Specification provided","required":true,"schema":{"$ref":"#/definitions/OauthCode"}}],"responses":{"200":{"description":"Successful request."},"default":{"description":"Invalid request.","schema":{"$ref":"#/definitions/Error"}}}}}},"definitions":{"organization":{"properties":{"id":{"type":"string"},"name":{"type":"string","default":""},"status":{"type":"integer","enum":[1,2,3],"default":1},"createDate":{"type":"string","format":"date"},"modifiedDate":{"type":"string","format":"dateTime"}},"required":["name"]},"application":{"properties":{"id":{"type":"string"},"name":{"type":"string"},"organization_id":{"type":"integer"},"detail":{"type":"string"},"status":{"type":"integer","enum":[1,2,3],"default":1},"createDate":{"type":"string","format":"date"},"modifiedDate":{"type":"string","format":"dateTime"}},"required":["name","detail","organization_id"]},"role":{"properties":{"id":{"type":"integer"},"name":{"type":"string"}},"required":["name"]},"permission":{"properties":{"id":{"type":"integer"},"role_id":{"type":"integer"},"name":{"type":"string"}},"required":["name","role_id"]},"setting":{"properties":{"id":{"type":"integer"},"label":{"type":"string"},"value":{"type":"string"}},"required":["label","value"]},"api_key":{"properties":{"id":{"type":"integer"},"organization_id":{"type":"integer"},"app_id":{"type":"integer"},"apikey":{"type":"string"},"api_secret":{"type":"string"},"expire_at":{"type":"integer"}},"required":["organization_id","app_id","apikey","api_secret"]},"user":{"properties":{"id":{"type":"string"},"email":{"type":"string"},"password":{"type":"string"},"role":{"type":"integer"},"app_id":{"type":"string"},"organization_id":{"type":"string"},"createDate":{"type":"string","format":"date"},"modifiedDate":{"type":"string","format":"dateTime"},"status":{"type":"integer","enum":[1,2,3],"default":1}},"required":["email","password","role","app_id","organization_id","status"]},"user_flow":{"properties":{"id":{"type":"integer"},"user_id":{"type":"integer"},"path":{"type":"string"},"createDate":{"type":"string","format":"date"}},"required":["user_id","path"]},"user_login_log":{"properties":{"id":{"type":"integer"},"user_id":{"type":"integer"},"login_date":{"type":"integer"},"attempt":{"type":"integer"},"ip":{"type":"string"}},"required":["user_id","login_date","attempt","ip"]},"user_operation":{"properties":{"id":{"type":"integer"},"name":{"type":"string"},"detail":{"type":"string"},"modifiedDate":{"type":"integer"}},"required":["name","detail"]},"Collection":{"properties":{"id":{"type":"integer"},"name":{"type":"string"},"json_schema":{"type":"string"}},"required":["name","json_schema"]},"Error":{"properties":{"message":{"type":"string"},"status":{"type":"string"}}},"OauthCode":{"properties":{"response_type":{"type":"string","enum":["code","client_credentials","password","refreshtoken"]},"client_id":{"type":"string"},"redirect_uri":{"type":"string"}},"required":["response_type","client_id","redirect_uri"]}}}';
}

validationServiceTest();