<?php 

function testDBAdapter (){
    try {
        $adapter = new MySqlDBAdapter([]);
    } catch(Exception $e){
        assert('true', 'Empty config should throw exception');
    }
    $config = [
        'db_host' => 'localhost',
        'db_user' => 'root',
        'db_password' => 'root',
        'db_dbName' => 'baas',
        'db_port' => 3306,
        'db_encoding' => 'utf8_unicode_ci'
    ];
    $adapter = new MySqlDBAdapter($config);
    assert('$adapter->getHost() == "localhost"', 'Make sure host is the same');
    assert('$adapter->getUser() == "root"', 'Make sure user is the same');
    assert('$adapter->getPassword() == "root"', 'Make sure password is the same');
    assert('$adapter->getDbName() == "baas"', 'Make sure dbName is the same');
    assert('$adapter->getPort() == 3306', 'Make sure port is the same');
    assert('$adapter->getEncoding() == "utf8_unicode_ci"', 'Make sure encoding is the same');
    try {
    $instance = $adapter->getInstance();
    } catch (Exception $e){
        echo $e->message;
        assert('false', 'Adapter not instance');
    }
}

testDBAdapter();