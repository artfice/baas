<?php 

function testMysql (){
        $config = [
            'db_host' => 'localhost',
            'db_user' => 'root',
            'db_password' => 'root',
            'db_dbName' => 'baas',
            'db_port' => 3306,
            'db_encoding' => 'utf8_unicode_ci'
        ];    
	   $mysql = new Mysql($config); 

       assert('$mysql->retrieve(["id" => 99999,"table" => "user"]) == NULL', 'Retrieving a row that doesn\'t exist');
       assert('$mysql->retrieve(["id" => 1,"table" => "setting"])["id"] == 1', 'Retrieving a row that does exist');
       assert('$mysql->retrieve(["id" => 1,"table" => "settings"])["id"] == false', 'Table does not exist');
       
       $pageResult = $mysql->retrieveAll(["table" => "setting", "page" => 1, "limit" => 1, "where" => []]);
	   
       assert('$pageResult["numPages"] > 0', 'num of pages exist');
       assert('$pageResult["page"] > 0', 'page number exist');
       assert('$pageResult["items"] > 0', 'items exist');
       
       assert('$mysql->raw(["query" => "SELECT * from settings"]) == false', 'Table doesn\'t exist');
       assert('count($mysql->raw(["query" => "SELECT * from setting"])) > 0', 'Raw query');
       assert('count($mysql->raw(["query" => "SELECT * from setting where id=999999"])) == 0', 'Raw query no records');
       
       $newRecord = $mysql->add(["table" => "setting", "fields" =>["label" => 'test', "value" => "r"]]);
       
       assert('is_numeric($newRecord)', 'Add Row');
       assert('$mysql->save(["table"=>"setting","id"=>$newRecord, "fields"=>["label" => "l1","value" => "l2"]])', 'update row');
       assert($mysql->delete(["table"=>"setting","id"=>$newRecord]), 'Delete row created');
       
}

testMysql();