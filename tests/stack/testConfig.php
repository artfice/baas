<?php
ini_set('display_errors', '1');
require 'flight/Flight.php';

Flight::path(__DIR__ . '/../..' . '/stack/');
Flight::path(__DIR__ . '/../..' . '/stack/api');
Flight::path(__DIR__ . '/../..' . '/stack/interfaces');
Flight::path(__DIR__ . '/../..' . '/stack/middleware');
Flight::path(__DIR__ . '/../..' . '/stack/repository');
Flight::path(__DIR__ . '/../..' . '/stack/services');
Flight::path(__DIR__ . '/../..' . '/stack/utility');
Flight::path(__DIR__ . '/../..' . '/stack/utility/auth');
Flight::path(__DIR__ . '/../..' . '/stack/utility/db');
Flight::path(__DIR__ . '/../..' . '/stack/utility/jwt');

$config = Configuration::getConfig();
Flight::set('config', $config);
Flight::set('swagger', json_decode(file_get_contents($config['swagger_json']), true));