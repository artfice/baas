var ApplicationModulePlugin = require('./plugin/ApplicationModulePlugin');

require('./controller');

var applicationModulePlugin = new ApplicationModulePlugin();
angular.module('application.plugin', [
    'application.controller'
]).config(function ($stateProvider) {
    var routes = applicationModulePlugin.getRoutes();
    for(var i = 0; i < routes.length; i++){
         $stateProvider.state(routes[i].name, routes[i].detail);
    }    
}).run(function(){});