var ModulePlugin = require('../../core/plugin/ModulePlugin');
var routes = require('../routes');
var ApplicationModule = function() {
    
};

var ApplicationModulePlugin = function() {
    ApplicationModulePlugin.super_.apply(this, [
        'App',
        'fa-app',
        routes,
        'application'
    ]);
};

Digi.inherits(ApplicationModulePlugin, ModulePlugin);

module.exports = ApplicationModulePlugin;