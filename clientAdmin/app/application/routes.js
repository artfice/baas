module.exports = [{
        stateName: 'application',
        url: '/application',
        templateUrl: 'app/application/template/application.html',
        controller: 'applicationController',
        controllerAs: 'vm',
    }];