require('./core');
require('./desktop');
var DesktopViewPlugin = require('./desktop/plugin/view/desktop/DesktopViewPlugin').DesktopViewPlugin;
var LoginViewPlugin = require('./desktop/plugin/view/login/LoginViewPlugin').LoginViewPlugin;

var app = angular.module('app', [
        'mui',
        'ui.router',
        'framework.core',
        'desktop.plugin'
]).run(['$rootScope', 'registryService', function($rootScope, registryService){
    registryService.register(new DesktopViewPlugin());
    registryService.register(new LoginViewPlugin());
}])
.config(function ($stateProvider, $urlRouterProvider) {
    // $urlRouterProvider.otherwise('/login');
});