var DynamicHtml = require('./uicomponent/DynamicHtml');
var PluginComponent = require('./uicomponent/PluginComponent');
var Purpose = require('./utility/Purpose');
var Digi = require('./Digi');
var HttpService = require('./service/HttpService');
var AccountService = require('./service/AccountService');
var ViewRegistry = require('./plugin/ViewRegistry');
var HostController = require('./view/page/host/HostController');
angular.module('framework.core', [])
.directive('dynamichtml', ['$compile', DynamicHtml])
.directive('pluginComponent', [PluginComponent])
.constant('PURPOSE', Purpose)
.service('digi', Digi)
.service('httpService', ['$http', '$q', '$rootScope', HttpService])
.service('registryService', [ViewRegistry])
.service('accountService', ['httpService', '$rootScope', AccountService])
.controller('hostController', ['accountService', '$scope', 'PURPOSE', HostController])
.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('app', {
        url : '',
        template: require('./view/page/host/host.html'),
        controller: 'hostController',
        controllerAs: 'vm'
    });
}).run([function(){
    
}]);