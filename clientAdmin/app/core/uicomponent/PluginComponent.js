var Digi = require('../Digi');
var PluginComponent = function () {
    return {
        restrict: 'E',
        template: '<div dynamichtml="component" class="dynamic" ng-if="ready"></div>',
        scope: {
            model: '=',
            options: '=',
            purpose: '=',
            prefer: '='
        },
        controller: ['registryService', '$scope', function (registryService, $scope) {
            $scope.ready = false;
            
            this.initialize = function (){
                $scope.ready = false;
                var plugins = undefined;
                
                if (Digi.isString($scope.model)) {
                    plugins = registryService.getPlugin($scope.model, 
                                                        $scope.purpose); 
                }

                if (Digi.Function.hasFunction($scope.model, 'getFieldValue')) {
                    plugins = registryService.getPlugin($scope.model.getFieldValue('_metadata'), 
                                                        $scope.purpose);   
                }
                
                if (!Digi.isDefined(plugins)) {
                    plugins = registryService.getPlugin($scope.model, 'viewPlugin');
                }
                
                if (!Digi.isDefined(plugins)) {
                    $scope.component = '';
                    console.log("PluginComponent: Plugin is not registered: ", $scope);
                } else {
                    if($scope.prefer != undefined) {
                        $scope.component = plugins[$scope.prefer].getView().getDirective();
                    } else {
                        $scope.component = plugins[0].getView().getDirective();
                    }
                }
                $scope.ready = true;
            }; 
            
            this.initialize();          
        }]
    };
};

module.exports = PluginComponent;