var BaseModel = require('./BaseModel');
var ModelFactory = function (schemaService, promiseLib) {
    this._schemaService = schemaService;
    this._promiseLib = promiseLib;
};

ModelFactory.prototype = {
	   create: function(schema, raw) {
           if(raw != undefined) {
               return this._instantiate(schema, raw);
           } else {
               return this._defineModel(schema);
           }
       },
       _instantiate: function (schema, raw) {
           
       },
       _defineModel: function(schemaType) {
           
        return this._schemaService.getSchema(schemaType).then(
            function(schema){
               var model = new BaseModel(schema, null);
               return model; 
            }
        );
       }  
};

module.exports = ModelFactory;