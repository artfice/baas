var Cardinality = require('../utility/Cardinality');
var ViewPlugin = function(view, viewType){
    this._view = view;
    this._viewType = viewType;
    this._cardinality = Cardinality.HAS_ONE;
};

ViewPlugin.prototype = {
    getCardinality: function (){
        return this._cardinality;
    },
    getType: function () {
        return 'view';
    },
    getView: function (){
        return this._view;
    },
    getViewType: function () {
        return this._viewType;
    }
};


module.exports = ViewPlugin;