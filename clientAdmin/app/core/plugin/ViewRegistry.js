var Cardinality = require('../utility/Cardinality');
var ViewRegistry = function () {
    this._pluginList = {
        viewPlugin: {}
    };
};

ViewRegistry.prototype = {
    _hasOneCardinality: function(cardinality) {
        return cardinality == Cardinality.HAS_ONE;
    },
    _hasManyCardinality: function(cardinality) {
        return cardinality == Cardinality.HAS_MANY;
    },
    _createNewPurpose: function(purpose) {
        if (this._pluginList[purpose] === undefined){
            this._pluginList[purpose] = [];
        }
    },
    _createNewModelTypeInPurpose: function(purpose, modelType){
        if (this._pluginList[purpose][modelType] === undefined) {
            this._pluginList[purpose][modelType] = [];
        }
    },
    register: function(plugin){
        switch(plugin.getType()){
        case 'view':
            this._setupViewPlugin(plugin);
            break;
        case 'model':
            this._setupModelViewPlugin(plugin);
            break;
        }
    },
    _setupModelViewPlugin: function(modelViewPlugin) {
        var purpose = modelViewPlugin.getPurpose(),
            modelType = modelViewPlugin.getModelType(),
            cardinality = modelViewPlugin.getCardinality();

        if (!purpose ||
            !modelType ||
            !(cardinality === Cardinality.HAS_ONE ||
            cardinality === Cardinality.HAS_MANY)
            ) {
            throw new Error('Cannot register invalid plugin: ', purpose, modelType, cardinality);
        }

        this._createNewPurpose(purpose);
        this._createNewModelTypeInPurpose(purpose, modelType);

        if (this._hasOneCardinality(cardinality)){
            this._pluginList[purpose][modelType] = [modelViewPlugin];
        }

        if (this._hasManyCardinality(cardinality)){
            this._pluginList[purpose][modelType].push(modelViewPlugin);
        }
    },
    _setupViewPlugin: function(viewPlugin) {
        var viewType = viewPlugin.getViewType();

        if (!viewType) {
            throw new Error('Cannot register invalid plugin: ', viewType);
        }

        if (this._pluginList.viewPlugin[viewType] === undefined) {
            this._pluginList.viewPlugin[viewType] = null;
        }

        this._pluginList.viewPlugin[viewType] = [viewPlugin];
    },
    deregister: function(modelType){
        var purposes = Object.keys(Purpose);
        for(var i = 0; i < purposes.length; i++){
            if (this._pluginList[purposes[i]].hasOwnProperty(modelType)){
                delete this._pluginList[purposes[i]][modelType];
            }
        }
    },
    getPlugin: function( modelType, purpose) {
        if (this._pluginList[purpose] &&
            this._pluginList[purpose][modelType]){
            return this._pluginList[purpose][modelType];
        }
    }
};

module.exports = ViewRegistry;