var ModulePlugin = function(name, 
                            icon, 
                            routes, 
                            startRoute){
	   this.routes = [];
       this.name = name;
       this.icon = icon;
       this.startRoute = startRoute; 
       
       for(var i = 0; i < routes.length; i++) {
         this.routes.push({
             name: routes[i].stateName, 
             detail: routes[i]
        });
       }
};

ModulePlugin.prototype = {
       getName: function () {
           return this.name;
       },
       getIcon: function () {
           return this.icon;
       },
       getStartingRoute: function() {
           return this.startRoute;
       },
       getRoutes: function () {
           return this.routes;
       }  
};


module.exports = ModulePlugin;