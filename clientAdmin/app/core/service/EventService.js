var EventService = function(){
    this.events = {};
};

EventService.prototype = {
    subscribe: function(key, cb){
        if(this.events.hasOwnProperty(key)) {
            this.events[key].push(cb);
        } else {
            this.events[key] = [];
        }
    },
    publish: function(key, args){
        if(this.events.hasOwnProperty(key)) {
            for(var i = 0; i < this.events[key]; i++) {
                this.events[key][i]();
            }
        }
    }
};

module.exports = EventService;