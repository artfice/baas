var Purpose = {
    CREATE: 1,
    VIEW: 2,
    MODULE: 3,
    EDIT: 4,
    PREVIEW: 5
};

module.exports = Purpose;