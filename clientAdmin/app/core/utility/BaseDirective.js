var BaseDirective = function(){
    this.name = '';
    this.renderObject = {
        replace: true
    };
};

BaseDirective.prototype = {
    getName: function () {
        return this.name;
    },
    getDefinition: function () {
        this.renderObject.controller = [function(){}];
        return this.renderObject;
    },
    setTemplate: function(template){
      if (typeof template == 'string'){
          this.renderObject.templateUrl = template;
      } else {
          this.renderObject.template = template.join(' ');
      } 
    },
    usingScope: function(scope){
      this.renderObject.scope = scope;  
    },
    usingVm: function(bindToController){
      this.renderObject.controllerAs = 'vm';
      this.renderObject.bindToController = bindToController;
    },
    usingPlugin: function(){
      this.renderObject.controllerAs = 'vm';
      this.renderObject.scope = false;
    },    
    getDirective: function() {
        var name = this.name.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
        return '<' + name + '></' + name + '>';
    }
};

module.exports = BaseDirective;