function Implement (target, source) {
    angular.extend(target.prototype, source);
}

module.exports = Implement;