var Cardinality = {
  HAS_ONE: 1,
  HAS_MANY: -1
};

module.exports = Cardinality;