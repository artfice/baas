var BaseDirective = require('../../../../core/utility/BaseDirective');
var ViewPlugin = require('../../../../core/plugin/ViewPlugin');
var Digi = require('../../../../core/Digi');
var DesktopView = function() {
    DesktopView.super_.apply(this,[]);
    this.name = 'desktopViewComponent';
};
Digi.inherits(DesktopView, BaseDirective);

DesktopView.prototype.getSideDrawer = function () {
    return [
        '<div id="sidedrawer" class="mui--no-user-select" ng-class="{hide: vm.hideSideBar}">',
        '<div id="sidedrawer-brand" class="mui--appbar-line-height mui--text-title">Manaknight</div>',
        '<div class="mui-divider"></div>',
        '<ul>',
        '  <li>',
        '    <strong ng-click="vm.toggleMenu(this)">Category 1</strong>',
        '    <ul>',
        '      <li><a href="#">Item 1</a></li>',
        '      <li><a href="#">Item 2</a></li>',
        '      <li><a href="#">Item 3</a></li>',
        '    </ul>',
        '  </li>',
        '  <li>',
        '  <li ng-mouseenter="showCategory=false">',
        '    <strong ng-click="showCategory=!showCategory">Category 1</strong>',
        '    <ul ng-class="{\'show-category\':showCategory}">',
        '      <li><a href="#">Item 1</a></li>',
        '      <li><a href="#">Item 2</a></li>',
        '      <li><a href="#">Item 3</a></li>',
        '    </ul>',
        '  </li>',
        '  <li>',        
        '   <strong>Category 2</strong>',
        '    <ul>',
        '      <li><a href="#">Item 1</a></li>',
        '      <li><a href="#">Item 2</a></li>',
        '      <li><a href="#">Item 3</a></li>',
        '    </ul>',
        '  </li>',
        '  <li>',
        '    <strong>Category 3</strong>',
        '    <ul>',
        '      <li><a href="#">Item 1</a></li>',
        '      <li><a href="#">Item 2</a></li>',
        '     <li><a href="#">Item 3</a></li>',
        '    </ul>',
        '  </li>',
        '</ul>',
        '</div>'        
    ].join(' ');    
};

DesktopView.prototype.getHeader = function () {
    return [
        '<header id="header">',
        '  <div class="mui-appbar mui--appbar-line-height">',
        '    <div class="mui-container-fluid">',
        '      <a class="sidedrawer-toggle mui--visible-xs-inline-block js-show-sidedrawer" ng-click="vm.closeSideBar()">☰</a>',
        '      <a class="sidedrawer-toggle mui--hidden-xs js-hide-sidedrawer" ng-click="vm.closeSideBar()">☰</a>',
        '      <span class="mui--text-title mui--visible-xs-inline-block">Manaknight</span>',
        '    </div>',
        '  </div>',
        '</header>'        
    ].join(' ');    
};

DesktopView.prototype.getContent = function() {
  return [
    '<div id="content-wrapper">',
    '    <div class="mui--appbar-height"></div>',
    '<div class="mui-container-fluid">',
    '  <br>',
    '  <h1>ManaKnight</h1>',
    '  <p>',
    '    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris',
    '  </p>',
    '  <p>',
    '    Aenean vehicula tortor a tellus porttitor, id elementum est tincidunt.',
    '  </p>',
    '  <p>',
    '    Quisque interdum facilisis consectetur. Nam eu purus purus. Curabitur in',
    '  </p>',
    '</div>',
    '</div>'      
  ].join(' ');  
};

DesktopView.prototype.getFooter = function () {
    return [
        '<footer id="footer">',
        '  <div class="mui-container-fluid">',
        '    <br>',
        '    Made with ♥ by <a href="https://www.muicss.com">MUI</a>',
        '  </div>',
        '</footer>'        
    ].join(' ');    
};

DesktopView.prototype.getDefinition = function () {
    this.usingPlugin();
    this.setTemplate([
        '<div class="desktop-plugin-component" ng-class="{\'hide-sidedrawer\':vm.hideSideBar}">',
        this.getSideDrawer(),
        this.getHeader(),
        this.getContent(),
        this.getFooter(),
        '</div>'
    ]);        
    this.renderObject.controller = [function(){
        this.hideSideBar = true;
        this.closeSideBar = function () {
            this.hideSideBar = !this.hideSideBar; 
        };
        
        this.toggleMenu = function(elm) {
          console.log(elm);  
        };
    }];
    return this.renderObject;

};

var DesktopViewPlugin = function(){
    DesktopViewPlugin.super_.apply(this, [
        new DesktopView(),
        'view.desktop'
    ]);
};

Digi.inherits(DesktopViewPlugin, ViewPlugin);

module.exports = {
    DesktopView: DesktopView,
    DesktopViewPlugin: DesktopViewPlugin
}