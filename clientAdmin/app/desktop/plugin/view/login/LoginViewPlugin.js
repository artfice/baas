var BaseDirective = require('../../../../core/utility/BaseDirective');
var ViewPlugin = require('../../../../core/plugin/ViewPlugin');
var Digi = require('../../../../core/Digi');
var LoginView = function() {
    LoginView.super_.apply(this,[]);
    this.name = 'loginViewComponent';
};

Digi.inherits(LoginView, BaseDirective);

LoginView.prototype.getDefinition = function () {
    this.usingPlugin();
    this.setTemplate([
        '<div style="width: 1024px;height: 1000px">',
            '<div style="width: 90%; margin-left: auto; margin-right: auto;">',
                '<form>',
                    '<input type="text" placeholder="email"/></br>',
                    '<input type="password" placeholder="password"/><br/>',
                    '<button ng-click="vm.login()">Log in</button>',
                '</form>',
            '</div>',
            '<div style="clear:both"></div>',
        '</div>'                    
    ]);        
    this.renderObject.controller = ['accountService', function(accountService) {
        this.login = function() {
            accountService.loggedIn();
        }
    }];
    return this.renderObject;
};

var LoginViewPlugin = function(){
    LoginViewPlugin.super_.apply(this, [
        new LoginView(),
        'view.login'
    ]);
};

Digi.inherits(LoginViewPlugin, ViewPlugin);

module.exports = {
    LoginView: LoginView,
    LoginViewPlugin: LoginViewPlugin
}