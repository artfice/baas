var LoginView = require('./plugin/view/login/LoginViewPlugin').LoginView;
var DesktopView = require('./plugin/view/desktop/DesktopViewPlugin').DesktopView;
var login = new LoginView();
var desktop = new DesktopView();

angular.module('desktop.plugin', [])
.directive(login.getName(), [function(){ return login.getDefinition()}])
.directive(desktop.getName(), [function(){ return desktop.getDefinition()}])
.run(function(){});