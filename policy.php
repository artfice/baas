<?php

$policies = [
    '/v1/authorize' => [
        'method' => 'post',
        'chain' => ['validation', 'authorization']
    ]
];


$middlewareMapping = [
    'validation'    => 'ValidationService',
    'authorization' => 'AuthorizationService'
];