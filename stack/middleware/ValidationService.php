<?php 

class ValidationService implements MiddlewareInterface {
    use ReflectMethod;
    private $_swagger;
    private $_testingMode;
    private $_typeMapping = [
        'string' => 'is_string',
        'integer' => 'is_int',
        'number' => 'is_numeric',
        'boolean' => 'is_bool'
    ];
    public function __construct ($testingMode=false) {
        $this->_swagger = Flight::get('swagger');
        $this->_testingMode = $testingMode;
    }
    
    public function execute($req, $res) {
        $input = Helper::getInput($req->get('options'));
        $result = $this->validate($req->get('swaggerUrl'),
                                  $req->get('method'),
                                  $input['body']);
                                  
        if ($result == APIResponses::VALID) {
            $res->set('body', $input['body']);
        } else {
            return APIResponses::send($result, $this->_testingMode);
        }
        
        return $res;
    }
    
    public function validate ($route, $method, $data) {
        $path = $this->_swagger['paths'][$route][$method];
        $parameters = $path['parameters'];
        foreach ($parameters as $key => $value) {
            $name = $value['name'];
            if (!array_key_exists('in', $value)) {
                return APIRESPONSE::INVALIDSWAGGERERROR;
            }
            $in = $value['in'];
            switch ($in) {
                case 'body':
                    $result = $this->_validateBody($value, $data);
                    break;
                case 'path':
                    $result = $this->_validatePath($value, $data['validation_path']);
                    break;
                case 'query':
                    $result = $this->_validateQuery($value, $data);
                    break;
                default:
                    break;
            }
            
            if ($result) {
                return $result;
            }          
        }
        
        return APIResponses::VALID;
    }
    
    private function _validateBody ($property, $data) {
        $schema = str_replace('#/definitions/', '', $property['schema']['$ref']);
        
        $model = $this->_swagger['definitions'][$schema]['properties'];
        $required = $this->_swagger['definitions'][$schema]['required'];
        // Helper::printr($required);
        // Helper::printr($model);
        
        $hasRequired = $this->_checkRequired($required, array_keys($data));
        
        if (!$hasRequired) {
            return APIResponses::REQUIREERROR;    
        }
        
        $hasValidTypes = $this->_checkTypes($model, $data);
        
        if (!$hasValidTypes) {
            return APIResponses::TYPEERROR;    
        }                
        
        $hasValidEnum = $this->_checkEnum($model, $data);
        
        if (!$hasValidEnum) {
            return APIResponses::ENUMERROR;    
        } 
        
        return false;         
    }

    private function _validateQuery ($property, $data) {
        $name = $property['name'];
        $type = $property['type'];
        $isRequired = $property['required'];
        
        if (!$this->_checkRequired([$name], array_keys($data))) {
            return APIResponses::REQUIREERROR;
        }
        
        if (!$this->_checkTypes([$name => $property], $data)) {
            return APIResponses::TYPEERROR;
        }        
        $value = $data[$name];
        
        return false;    
    }
    
    private function _validatePath($property, $data) {
        $name = $property['name'];
        $type = $property['type'];
        $isRequired = $property['required'];
        
        if (!$this->_checkRequired([$name], array_keys($data))) {
            return APIResponses::REQUIREERROR;
        }
        
        if (!$this->_checkTypes([$name => $property], $data)) {
            return APIResponses::TYPEERROR;
        }        
        $value = $data[$name];
        
        return false;        
    }
    
    private function _checkRequired ($required, $data) {
        $numRequiredFieldFound = count(array_intersect($required, $data));
        $numRequiredField = count($required);
        return $numRequiredFieldFound === $numRequiredField;
    }
    
    public function _checkTypes ($model, $data) {
        $types = $this->_typeMapping;
        foreach ($model as $key => $value) {
            $type = $value['type'];
            $method = $types[$type];
            
            if (!$method($data[$key])) {
                return false;
            }
        }
        return true;
    }   

    public function _checkEnum ($model, $data) {
        foreach ($model as $key => $value) {
            if (array_key_exists('enum', $value)){
                if (!in_array($data[$key], $value['enum'])) {
                    return false;
                }
            }
        }
        return true;
    }       
    
}