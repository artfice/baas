<?php 

/*
 * For the Authorize Endpoint, which requires the user to authenticate and redirects back to the
 * client with an authorization code (Authorization Code grant type) or access token (Implicit grant type).
 */
class AuthorizationService implements MiddlewareInterface {
    private $_allowedResponseCode = ['token', 'code'];
	private $_testingMode; 
    private $_message;
    private $_appRepo;
    private $_tokenService;
    private $_app;
    private $_expireAt;
    
    public function __construct($testingMode=false) {
        $this->_testingMode = $testingMode;
        $this->_message = APIResponses::VALID;
        $db = Flight::get('db');
        $config = Flight::get('config');
        $this->_appRepo = new AppRepo($db);
        $this->_tokenService = new TokenService();
        $this->_app = null;
        $this->_expireAt = $config['code_expire_at'];
    }
    
    public function execute($req, $res) {
        $input = Helper::getInput();
        $authorization = $input['authorization'];
        $result = $this->validateAuthorizeRequest($input);
        $res->set('body',[
            'authorization_token' => $result
        ]);
        return $res;
    }
    /**
     * Receives a request object for an authorize request, returns a response object with the appropriate response
     */
    public function handleAuthorizeRequest () {
        
    }
    
    /**
     * Receives a request object, returns false if the incoming request is not a valid Authorize Request. 
     * If the request is valid, returns an array of retrieved client details together with input. 
     * Applications should call this before displaying a login or authorization form to the user
     */
    public function validateAuthorizeRequest ($input) {
        if (!$this->_validateResponseCode($input)) {
            return $this->getMessage();
        }
        
        if (!$this->_validateClientId($input)) {
            return $this->getMessage();
        }        
        
        if ($input['body']['response_type'] == 'code') {
            $now = new DateTime();
            $now->add(DateInterval::createFromDateString("{$this->_expireAt} seconds"));
            return $this->_tokenService->generateToken('code', $now->format('Y-m-d G:i:s'), $this->_app['slug']);
        } else {

        }     
        
        return APIResponses::VALID;    
    }
    /**
     @return $messsage how authorization handles it
     */
    public function getMessage() {
        return $this->_message;
    }
    
    private function _validateResponseCode($input) {
        $this->_responseCode = $input['body']['response_type'];
        
        if(strlen($this->_responseCode) < 1){
           $this->_message = APIResponses::REQUIREERROR;
           return false; 
        }
        
        if (!in_array($this->_responseCode, $this->_allowedResponseCode)){
           $this->_message = APIResponses::REQUIREERROR;
           return false; 
        }
        
        return true;
    }
    
    private function _validateClientId($input) {
        $clientId = $input['body']['client_id'];
	    $app = $this->_appRepo->find($clientId);   
        
        if (!$app) {
            $this->_message = APIResponses::CLIENTNOTFOUND;
            return false;
        } 
        
        $this->_app = $app;
        
        return true;
         
    }
}