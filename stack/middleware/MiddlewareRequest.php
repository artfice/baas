<?php

class MiddlewareRequest {
    private $_url;
    private $_method;
    private $_options;
    private $_swaggerUrl;
    public function __construct ($url, $method, $options = []) {
        $this->_url = $url;
        $this->_method = $method;
        $this->_swaggerUrl = $options['swagger_url'];
        unset($options['swagger_url']);
        $this->_options = $options;
    }
    
    public function get ($key) {
        $field = "_$key";
        return $this->$field;    
    }
        
    public function getResult () {
        return [
            'url' => $this->_url,
            'method' => $this->_method,
            'options' => $this->_options,
            'swaggerUrl' => $this->_swaggerUrl,
        ];
    }
}