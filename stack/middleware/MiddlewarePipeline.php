<?php

class MiddlewarePipeline {
    private $_res;
    private $_req;
    private $_policies;
    private $_testingMode;
    public function __construct ($req, $testingMode = false) {
        $this->_res = new MiddlewareResponse();
        $this->_req = $req;
        $this->_policies = Flight::get('policy');
        $this->_middlewareMapping = Flight::get('middlewareMapping');
        $this->_testingMode = $testingMode;
    }
    
    public function execute () {
        $routes = array_keys($this->_policies);
        $url = $this->_req->get('url');

        if (!in_array($url, $routes)) {
            error_log("Error : URL does not exist in policy $url");
            return APIResponses::send(APIResponses::POLICYERROR, $this->_testingMode);
        }
        
        if ($this->_req->get('method') != $this->_policies[$url]['method']) {
            error_log("Error : Method does not exist in policy $url " . $this->_req->get('method'));
            return APIResponses::send(APIResponses::POLICYERROR, $this->_testingMode);
        }
        
        $chain = $this->_policies[$url]['chain'];
        
        foreach ($chain as $value) {
	           $class = $this->_middlewareMapping[$value];
               $instance = new $class();
               $this->_res = $instance->execute($this->_req, $this->_res); 
        }

        return $this->_res->getResult();
    }
}