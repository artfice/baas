<?php

class MiddlewareResponse {
    private $_data = [];
    public function __construct () {
        
    }
    public function get ($key) {
        return $this->_data[$key];    
    }
    
    public function set ($key, $value) {
        $this->_data[$key] = $value;    
    }
        
    public function getResult () {
        return $this->_data;
    }
}