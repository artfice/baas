<?php 
class Configuration {
    public function __construct(){}
    public static function getConfig(){
        $env = file_get_contents('.env');
        $envLines = explode(';', $env);
        $config = [];
        
        foreach($envLines as $row) {
            
            $part = explode('=', $row);
            
            if (strlen($part[0]) > 0){
                if (is_numeric($part[1])){
                    $part[1] = $part[1] + 0;
                }
                if (is_bool($part[1])){
                    $part[1] = ($part[1] === 'true')?true:false;
                }
                $config[trim($part[0])] = $part[1];    
            }
            
        }
        
        if (!empty($config['flight.log_errors'])) {
            Flight::set('flight.log_errors', $config['flight.log_errors']);
        }

        return $config;
    }
    
    public static function selectDB ($config) {
        switch ($config['db_type']) {
            case 'mysql':
                $db = new Mysql($config); 
                break;
            case 'postgres':
                # code...
                break;
            case 'mongodb':
                # code...
                break;                            
            default:
                throw new Exception('Database Cannot Be Found');
                break;
        }
        
        return $db;
    }
}