<?php 
require 'stack/utility/jwt/JWT.php';

// require 'stack/utility/jwt/SignatureInvalidException.php';
// use \Firebase\JWT\JWT;

//authorization code grant
Flight::route('GET /v1/authorizes', function() use ($config) {
    //check for authorization_code
    //return error, message 
    //then show login screen
    //if authenticated, redirect
    


$key = "example_key";
$token = array(
    "iss" => "http://example.org",
    "aud" => "http://example.com",
    "iat" => 1356999524,
    "nbf" => 1357000000
);

/**
 * IMPORTANT:
 * You must specify supported algorithms for your application. See
 * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
 * for a list of spec-compliant algorithms.
 */
 try {
$jwt = JWT::encode($token, $key);
$decoded = JWT::decode($jwt.'a', $key, array('HS256'));



/*
 NOTE: This will now be an object instead of an associative array. To get
 an associative array, you will need to cast it as such:
*/

$decoded_array = (array) $decoded;
var_dump($decoded_array);
} catch (Exception $e){
    var_dump($e->getMessage());
    var_dump('exception');
}
/**
 * You can add a leeway to account for when there is a clock skew times between
 * the signing and verifying servers. It is recommended that this leeway should
 * not be bigger than a few minutes.
 *
 * Source: http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#nbfDef
 */
JWT::$leeway = 60; // $leeway in seconds
$decoded = JWT::decode($jwt, $key, array('HS256'));    
    // Flight::halt(200, 'Be right back...');
});

Flight::route('POST /v1/token/access', function() use ($config) {
    //check for authorization_code
    //return back an authorization token
    //status_code, message
    Flight::halt(200, 'Be right back...');
});

Flight::route('POST /v1/token/refresh', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});

//client_credentials
//access_token

// /token_info
//get session from access_token (owner_id, owner_type or role, access_token, client_id, scopes)

// /users
// $server->getAccessToken()->hasScope('blah')

//curl -u testclient:testpass "http://localhost/token.php" -d 'grant_type=password&username=someuser&password=somepassword'
//{"access_token":"206c80413b9a96c1312cc346b7d2517b84463edd","expires_in":3600,"token_type":"bearer","scope":null}

//curl -u testclient:testpass "http://localhost/token.php" -d 'grant_type=client_credentials'
//{"access_token":"6f05ad622a3d32a5a81aee5d73a5826adb8cbf63","expires_in":3600,"token_type":"bearer","scope":null}

//curl -u testclient:testpass "http://localhost/token.php" -d 'grant_type=refresh_token&refresh_token=c54adcfdb1d99d10be3be3b77ec32a2e402ef7e3'
//{"access_token":"0e9d02499fe06762ecaafb9cfbb506676631dcfd","expires_in":3600,"token_type":"bearer","scope":null}

//Authorize Endpoint - The user is redirected here by the client to authorize the request
//Token Endpoint - The client makes a request to this endpoint in order to obtain an Access Token
//Resource Endpoint(s) - The client requests resources, providing an Access Token for authentication token. 


/*
http://brentertainment.com/oauth2/lockdin/authorize?response_type=code&client_id=demoapp&redirect_uri=http://google.com&state=b6960d7f941d366c0bc53a20e753e237

Welcome to the OAuth2.0 Server!

You have been sent here by demoapp. demoapp would like to access the following data:

friends
memories
hopes, dreams, passions, etc.
sock drawer
It will use this data to:

integrate with friends
make your life better
miscellaneous nefarious purposes
Click the button below to complete the authorize request and grant an Authoriation Code to demoapp.

http://brentertainment.com/oauth2/client/receive_authcode?code=4cb873fce87dbd190303437e05c7f6c80608945f&state=b6960d7f941d366c0bc53a20e753e237

Authorization Code: 4cb873fce87dbd190303437e05c7f6c80608945f  

http://brentertainment.com/oauth2/client/request_token/authorization_code?code=4cb873fce87dbd190303437e05c7f6c80608945f

 Access Token: 775b025bc85666fe131f4004f92f09deabe948aa  
 
 http://brentertainment.com/oauth2/client/request_resource?token=775b025bc85666fe131f4004f92f09deabe948aa
*/


/*
Implicit grant type
upon request, the access token is in url
http://brentertainment.com/oauth2/lockdin/authorize?response_type=token&client_id=demoapp&redirect_uri=http://google.com&state=b6960d7f941d366c0bc53a20e753e237
*/

/*
User credential grant type
curl -v "http://brentertainment.com/oauth2/lockdin/token" \
    -d "grant_type=password&client_id=demoapp&client_secret=demopass&username=demouser&password=testpass"
*/

/*
refresh token grant type
http://brentertainment.com/oauth2/lockdin/authorize?response_type=code&client_id=demoapp&redirect_uri=http://google.com%3Fshow_refresh_token%3D1&state=b6960d7f941d366c0bc53a20e753e237

http://brentertainment.com/oauth2/client/request_token/authorization_code?code=b26ebbe19f5ba371f886a67307f7c8fa92cbecb8&show_refresh_token=1

http://brentertainment.com/oauth2/client/request_token/refresh_token?refresh_token=057495f9c733f7e24dc120a47fb2e1c6fc46b6cf
*/

/*
open ID connect
http://brentertainment.com/oauth2/lockdin/authorize?response_type=code&client_id=demoapp&redirect_uri=http://google.com&scope=openid&state=b6960d7f941d366c0bc53a20e753e237&nonce=808935813
Because you requested the token using scope=openid, you have also received an ID token!

  ID Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJicmVudGVydGFpbm1lbnQuY29tIiwic3ViIjpudWxsLCJhdWQiOiJkZW1vYXBwIiwiaWF0IjoxNDU1NDMyNDg1LCJleHAiOjE0NTU0MzYwODUsImF1dGhfdGltZSI6MTQ1NTQzMjQ4NSwibm9uY2UiOiI4MDg5MzU4MTMifQ.KPkq3R34cr_LxReGarWnoeMG7m2I-gwYvF1XgSnPgSZc9Mh8Zxr5UZ5zvg2E-s_VQfqJSc4_LlRD_GoOc5PjxLiwW0hA_4XPvAw_GQ6bassAoREFj0yXNi1gjTDOBwXtyo7EiWEH5RCuri5CkY_oUxKV2V5eiR0_KURjXJaQq1ExruQ5LGlOIK7Q0eZMbppG_hRLbzE6sf1Y8lmEOromzpxxq1-TPrpWb8SMd_V0aQ-392NTlQMYF6MAHPB7I2zQDFgju2z4BA_lHht_eAcvlj21WOExjq4qoBhwT2fxYqAlXIQrt7OsMOj2_UBguDCFex8JfVj3TOQJDVPqKBvwFQ  
This token contains the following information about the user:

{
    "iss": "brentertainment.com",
    "sub": null,
    "aud": "demoapp",
    "iat": 1455432485,
    "exp": 1455436085,
    "auth_time": 1455432485,
    "nonce": "808935813"
}
*/




/*
Authorization Code
https://api.mysite.com/authorize?response_type=code&client_id=TestClient&redirect_uri=https://myredirecturi.com/cb

https://myredirecturi.com/cb?code=SplxlOBeZQQYbYS6WxSbIA&state=xyz

curl -u TestClient:TestSecret https://api.mysite.com/token -d 'grant_type=authorization_code&code=xyz'
{"access_token":"03807cb390319329bdf6c777d4dfae9c0d3b3c35","expires_in":3600,"token_type":"bearer","scope":null}
*/

/*
Implicit
t is optimized for public clients, such as those implemented in javascript or on mobile devices, where client credentials cannot be stored.

https://api.mysite.com/authorize?response_type=token&client_id=TestClient&redirect_uri=https://myredirecturi.com/cb
https://myredirecturi.com/cb#access_token=2YotnFZFEjr1zCsicMWpAA&state=xyz&token_type=bearer&expires_in=3600
*/

/*
 User Credentials grant type (a.k.a. Resource Owner Password Credentials) is used when the user has a trusted 
 relationship with the client, and so can supply credentials directly.
 
 curl -u TestClient:TestSecret https://api.mysite.com/token -d 'grant_type=password&username=bshaffer&password=brent123'
 
 curl https://api.mysite.com/token -d 'grant_type=password&client_id=TestClient&username=bshaffer&password=brent123
 {"access_token":"03807cb390319329bdf6c777d4dfae9c0d3b3c35","expires_in":3600,"token_type":"bearer","scope":null}
 */
 
 /*
 Client Credentials grant type is used when the client is requesting access to protected resources under its 
 control (i.e. there is no third party).

curl https://api.mysite.com/token -d 'grant_type=client_credentials&client_id=TestClient&client_secret=TestSecret'
 */
 
 /*
 The Refresh Token grant type is used to obtain additional access tokens in order to prolong the client’s 
 authorization of a user’s resources.
 
 Note: Refresh tokens are only provided when retrieving a token using the Authorization Code or User Credentials grant types.

refresh_token_lifetime
time before refresh token expires
Default: 1209600 (14 days)

curl -u TestClient:TestSecret https://api.mysite.com/token -d 'grant_type=password&username=bshaffer&password=brent123'
{
    "access_token":"2YotnFZFEjr1zCsicMWpAA",
    "expires_in":3600,
    "token_type": "bearer",
    "scope":null,
    "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
}

curl -u TestClient:TestSecret https://api.mysite.com/token -d 'grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA'

{"access_token":"03807cb390319329bdf6c777d4dfae9c0d3b3c35","expires_in":3600,"token_type":"bearer","scope":null}
 */
 
 /*
 The JWT Bearer grant type is used when the client wants to receive access tokens without transmitting sensitive
  information such as the client secret. This can also be used with trusted clients to gain access to user resources
   without user authorization.
   

 * Generate a JWT
 *
 * @param $privateKey The private key to use to sign the token
 * @param $iss The issuer, usually the client_id
 * @param $sub The subject, usually a user_id
 * @param $aud The audience, usually the URI for the oauth server
 * @param $exp The expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 * @return string

function generateJWT($privateKey, $iss, $sub, $aud, $exp = null, $nbf = null, $jti = null)
{
    if (!$exp) {
        $exp = time() + 1000;
    }

    $params = array(
        'iss' => $iss,
        'sub' => $sub,
        'aud' => $aud,
        'exp' => $exp,
        'iat' => time(),
    );

    if ($nbf) {
        $params['nbf'] = $nbf;
    }

    if ($jti) {
        $params['jti'] = $jti;
    }

    $jwtUtil = new OAuth2\Encryption\Jwt();

    return $jwtUtil->encode($params, $privateKey, 'RS256');
}   

$private_key = file_get_contents('id_rsa');
$client_id   = 'TestClient';
$user_id     = 'User1';
$grant_type  = 'urn:ietf:params:oauth:grant-type:jwt-bearer';

$jwt = generateJWT($private_key, $client_id, $user_id, 'https://api.mysite.com');

passthru("curl https://api.mysite.com/token -d 'grant_type=$grant_type&assertion=$jwt'");

JWT Access Tokens provide a way to create and validate access tokens without requiring a central storage 
such as a database. This decreases the latency of the OAuth2 service when validating Access Tokens.
HEADER.PAYLOAD.SIGNATURE

The HEADER is a Base64 URL Safe encoding of the following JSON: {"typ": "JWT", "alg":"RS256"}

{
    "id":"b08e1069f585ccc124ec1e694b2a609f1153caf8",
    "token_type":"bearer",
    "expires":"1379982305",
    "user_id":"THE_USER_ID",
    "client_id":"THE_CLIENT_ID",
    "scope": "onescope twoscope"
}

id - the internal id of the token
token_type - the kind of token (bearer)
expires - UNIX timestamp when the token expires
user_id - the id of the user for which the token was released
client_id - the id of the client who requested the token
scope - space-separated list of scopes for which the token is issued

# private key
$ openssl genrsa -out privkey.pem 2048

# public key
$ openssl rsa -in privkey.pem -pubout -out pubkey.pem
curl -i -v http://localhost:3000/token.php -u 'CLIENT_ID:CLIENT_SECRET' -d "grant_type=client_credentials"

CREATE TABLE oauth_public_keys (client_id VARCHAR(80), public_key VARCHAR(8000), private_key VARCHAR(8000), encryption_algorithm VARCHAR(80) DEFAULT "RS256")

$token = json_decode($curlResponse);

$jwt_access_token = $token['access_token'];

$separator = '.';

if (2 !== substr_count($jwt_access_token, $separator)) {
    throw new Exception("Incorrect access token format");
}

list($header, $payload, $signature) = explode($separator, $jwt_access_token);

$decoded_signature = base64_decode($signature);

// The header and payload are signed together
$payload_to_verify = utf8_decode($header . $separator . $payload);

// however you want to load your public key
$public_key = file_get_contents('/path/to/pubkey.pem');

// default is SHA256
$verified = openssl_verify($payload_to_verify, $decoded_signature, $public_key, OPENSSL_ALGO_SHA256);

if ($verified !== 1) {
    throw new Exception("Cannot verify signature");
}

// output the JWT Access Token payload
var_dump(base64_decode($payload));
 */
 
 /*
 reference
 
 http://bshaffer.github.io/oauth2-server-php-docs/cookbook/
 */
 
 /*
 jwt
{
  iat: 1416929061,
  jti: "802057ff9b5b4eb7fbb8856b6eb2cc5b",
  scopes: {
    users: {
      actions: ['read', 'create']
    },
    users_app_metadata: {
      actions: ['read', 'create']
    }
  }
}
// intercept all calls to API and validae the token
app.use('/api', express_jwt({secret: SECRET, userProperty: 'token_payload'}));

// for POST /user/repo validate that there is a scope `repo` or `public_repo`
app.post('/api/user/repo',
        check_scopes(['repo', 'public_repo']),
        function(req, res, next) {
    // create a repo
    ....
});

function check_scopes(scopes) {
  return function(req, res, next) {
    //
    // check if any of the scopes defined in the token,
    // is one of the scopes declared on check_scopes
    //
    var token = req.token_payload;
    for (var i =0; i<token.scopes.length; i++){
      for (var j=0; j<scopes.length; j++){
          if(scopes[j] === token.scopes[i]) return next();
      }
    }

    return res.send(401, 'insufficient scopes')
  }
} 
 */