<?php 

Flight::route('GET /authorize', function() {
    // $input = Helper::getInput();
    // APIResponses::send($validation->validate('/authorize', 'post', $input['body']));
    //call validation
    //check if code is inside db for this client_id
    //if it is, redirect to home
    //no then show page
   // include_once 'client/auth/authorize.php';
    //response_type=code&client_id=TestClient&redirect_uri=https://myredirecturi.com/cb
    $logo = "Manaknight";
    $appName = "Bidding Auction";
    $scope = ["profile", "friends", "photos"];
    include_once 'client/auth/authorizeScope.php';
});    

Flight::route('GET /authorize/signin', function() {
    include_once 'client/auth/signin.html';
    //response_type=code&client_id=TestClient&redirect_uri=https://myredirecturi.com/cb
});    

Flight::route('POST /v1/authorize', function() {
    $req = new MiddlewareRequest('/v1/authorize', 'post', ['swagger_url' => '/authorize']);
    $pipeline = new MiddlewarePipeline($req);
    $res = $pipeline->execute();
    Flight::json($res['body'], 200);
    //response_type=code&client_id=TestClient&redirect_uri=https://myredirecturi.com/cb
});    

// Flight::route('/private/mysql/admin', function() {
//     $basicAuth = new BasicAuth($config, 'phpadmin');
//     $basicAuth->authenticate();    
//     include_once 'clientAdmin/adminer.php';
// }); 

// Flight::route('POST /v1/authorize/requestToken', function() {
//     $input = Helper::getInput();
//     APIResponses::send($validation->validate('/authorize/requestToken', 'post', $input['body']));
//     Helper::printr($input);
//     $authorization = new AuthorizationService($config);
    
//     //response_type=code&client_id=TestClient&redirect_uri=https://myredirecturi.com/cb
// });    