<?php 

Flight::route('GET /v1/app/@appId/schema', function($appId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('GET /v1/app/@app_id/schema/@schemaId', function($appId, $schemaId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('POST /v1/app/@appId/schema', function($appId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('DELETE /v1/app/@app_id/schema/@schemaId', function($appId, $schemaId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('PUT /v1/app/@app_id/schema/@schemaId', function($appId, $schemaId) use ($config) {
    Flight::halt(200, 'Be right back...');
});