<?php 

Flight::route('GET /v1/organization', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('GET /v1/organization/@id', function($id) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('POST /v1/organization', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('PUT /v1/organization/@id', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});