<?php 

Flight::route('GET /v1/user', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('GET /v1/user/@id', function($id) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('POST /v1/user', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('PUT /v1/user/@id', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('POST /v1/signup', function() use ($config) {
    Flight::halt(200, 'Be right back...');
});