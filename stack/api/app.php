<?php 

Flight::route('GET /v1/organization/@organizationId/app', function($organizationId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('GET /v1/organization/@organization_id/app/@appId', function($organizationId, $appId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('POST /v1/organization/@organizationId/app', function($organizationId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('DELETE /v1/organization/@organization_id/app/@appId', function($organizationId, $appId) use ($config) {
    Flight::halt(200, 'Be right back...');
});

Flight::route('PUT /v1/organization/@organization_id/app/@appId', function($organizationId, $appId) use ($config) {
    Flight::halt(200, 'Be right back...');
});