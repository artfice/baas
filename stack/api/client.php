<?php 

Flight::route('/', function() {
    $config = Flight::get('config');
    $basicAuth = new BasicAuth($config, 'swagger');
    $basicAuth->authenticate();
    include_once 'client/index.html';
});

Flight::route('/admin', function() {
    include_once 'clientAdmin/index.html';
});

Flight::route('/member', function() {
    include_once 'clientMember/index.html';
});

Flight::map('error', function(Exception $ex) {
    // Handle error
    $config = Flight::get('config');
    error_log('ERROR ' . $ex->getTraceAsString());
    if ($config['type'] == 'dev') {
        echo "<pre>" . print_r($ex, true) . "</pre>";
    } else {
        Flight::json([
        'message' => 'server error'
    ], 500);        
    }

});

Flight::map('notFound', function(){
  Flight::json([
       'message' => 'api not found'
   ], 404);
});