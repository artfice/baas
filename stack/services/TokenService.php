<?php 

/**
 * For the Token Endpoint, which uses the configured Grant Types to return an access token to the client.
 */
class TokenService {
    private $_tokenRepo;
    private $_testingMode;
    public function __construct ($db=null, $testingMode=false) {
        if (!$db) {
            $db = Flight::get('db');
        }
        $this->_testingMode = $testingMode;
        $this->_tokenRepo = new TokenRepo($db);
    }
    
    /**
     * Generate a Token
     * @param $type [code, access, refresh, other]
     * @param $expireAt integer
     * @param $userId integer
     */
    public function generateToken($type, $expireAt, $userId) {
        $data = [
            'type' => $type,
            'expire_at' => $expireAt,
            'identifier' => $userId            
        ];
        
        switch ($type) {
            case 'code':
                $data['token'] = sha1(Security::randString(25,true));
                break;
            default:
                break;
        }
        
        $result = $this->_tokenRepo->add($data);
        error_log(print_r($data,true));
        if (!$result) {
            error_log('Error : Token not generated ' . "$type $expireAt $userId");
            return APIResponses::send(APIResponses::ERRORNOTFOUND, $this->_testingMode);
        }
        
        return $data['token'];
    }
    
    /**
     * Receives a request object for a token request, returns a token if the request is valid.
     */
    public function grantAccessToken () {}
    
    /**
     * Receives a request object for a token request, returns a response object for the appropriate response.
     */
    public function handleTokenRequest () {}
}