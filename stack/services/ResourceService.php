<?php 
/**
 * For any resource request (i.e. API Call) requiring oauth2 authentication. 
 * The controller will validate the incomming request, and then allow the application to 
 * serve back the protected resource.
 */
class ResourceService {
    public function __construct () {
        
    }
    
    /**
     * Receives a request object for a resource request, finds the token if it exists, and 
     * returns a Boolean for whether the incomming request is valid
     */
    public function verifyResourceRequest () {
        
    }
    
    /**
     * Takes a request object as an argument and returns the token data if applicable, or null if the token is invalid
     */
    public function getAccessTokenData () {
        
    }
}