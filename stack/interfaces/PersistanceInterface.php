<?php 
interface PersistanceInterface {
    public function retrieve(array $parameters);
    public function retrieveAll(array $parameters);
    public function raw($parameters);
    public function save(array $parameters);
    public function add(array $parameters);
    public function delete(array $parameters);
}