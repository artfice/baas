<?php 
interface MiddlewareInterface {
    public function execute($req, $res);
}