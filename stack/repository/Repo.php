<?php 

class Repo {
    protected $_db;
	protected $_table;
    protected $_slug = false;
     
    public function __construct($db) {
        $this->_db = $db;    
    }    
        
    public function add($obj) {
        return $this->_db->add(array(
            'fields' => $obj,
            'table' => $this->_table
            ),
            ($this->_slug)?true:false);
    }   
            
    public function find($id) {
        return $this->_db->retrieve(array(
            'id' => $id,
            'table' => $this->_table
            ),
            ($this->_slug)?true:false);
    }
    
    public function pageFind($page=1, $limit=10, $where=[], $sort='', $order='') {
        return $this->_db->retrieveAll(array(
	        'page' => $page,
            'limit' => $limit,
            '$where' => $where,
            '$sort' => $sort,
            '$order' => $order,    
            'table' => $this->_table
            ),
            ($this->_slug)?true:false);
    }
        
    public function remove($obj) {
        return $this->_db->delete(array(
            'table' => $this->_table,
            'id' => ($this->_slug)?$obj['slug']:$obj['id']
            ),
            ($this->_slug)?true:false);
    }
    
    public function save($obj) {
        return $this->_db->save(array(
            'fields' => $obj,
            'table' => $this->_table,
            'id' => ($this->_slug)?$obj['slug']:$obj['id']
            ),
            ($this->_slug)?true:false);
    }        
}