<?php 

class Mysql implements PersistanceInterface {
    private $_db;
    
    public function __construct($config) {
        $adapter = new MySqlDBAdapter($config);
        $this->_db = $adapter->getInstance();
        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    /**
    * Retrieve One Row from the database
    *
    * Retrieve one row from database by id
    *
    * @since 0.0.0
    * @access public
    *
    * @param array $parameter ['id' =>$id, 'table' => 'users']
    * @param boolean $slug if slug true then select by that field    
    * @return array object of the row
    * @return NULL if cannot find it    
    * @return false if table doesn't exist    
    */
    public function retrieve(array $parameters, $slug=false) {
        try {
        $table = $parameters['table'];
        $id = $parameters['id'];
        $rows = [];
        $field = ($slug)?'slug':'id';
        if ($field == 'slug') {
            $id = '"' . $id . '"';
        }
        $result = $this->_db->query("SELECT * FROM $table WHERE $field=$id");
        
        if (!$result) {
            error_log('Error ' . ' Mysql-retrieve table does not exist ' . json_encode($parameters));
            return false;
        }
        
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $rows[] = $row;
        }
        
        if (count($rows) < 1) {
            return NULL;
        }
        return $rows[0];
        } catch(PDOException $e) {
            error_log('ERROR' . ' Mysql->retrieve get error ' . $e->getMessage() .  "\n");
            return false;
        }
    }
    
    /**
    * Retrieve Paged Rows
    *
    * Retrieved all rows based on paged number
    *
    * @since 0.0.0
    * @access public
    *
    * @param array $parameter ['table' => 'users', 'page' => 1, 'limit' => 10, 'where' => [id, value], 'sort', 'order']
    * @return array of the row
    * @return NULL number of records failed    
    * @return false if table doesn't exist
    */
    public function retrieveAll(array $parameters) {
        try {
        $table = $parameters['table'];
        $page = $parameters['page'];
        $limit = $parameters['limit'];
        
        // $sort = $parameters['sort'];
        // $order = $parameters['order'];
        $where = $parameters['where'];
        
        $whereStr = $this->processWhere($where, 
            ' AND ', 
            function($key, $value){
                $op = $value['op'];
                $val = $value['value'];
                return "$key $op $val"; 
        });
        
        $offset = ($page - 1) * $limit;
        
        $rows = [];
        
        $result = $this->_db->query("SELECT COUNT(*) as num FROM $table WHERE $whereStr");
        if (!$result) {
            error_log('Error ' . ' Mysql-retrieve table does not exist ' . json_encode($parameters));
            return false;
        }
        
        $num = $result->fetch(PDO::FETCH_ASSOC);
        
        if (count($num) < 1) {
            return NULL;
        }

        $numRecords = $num['num'];
        
        if (($page * $limit) > $numRecords) {
            return ['items' => [], 'page' => $page, 'numPages' => $numRecords];
        }
        
        $result = $this->_db->query("SELECT * FROM $table WHERE $whereStr LIMIT $limit OFFSET $offset");

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $rows[] = $row;
        }
        
        return ['items' => $rows, 'page' => $page, 'numPages' => $numRecords];
                    
        } catch(PDOException $e) {
            error_log('ERROR' . ' Mysql->retrieve All error'   . json_encode($parameters) . ' ' . $e->getMessage() . "\n");
            return false;
        }
        
    }
    
    /**
    * Raw Mysql Query.
    *
    * Raw Mysql Query
    *
    * @since 0.0.0
    * @access public
    *
    * @param string $parameter ['query' => 'string']
    * @return array results
    * @return false if table doesn't exist    
    */    
    public function raw($parameters) {
        try {
            $query = $parameters['query'];
            $rows = [];
            
            $result = $this->_db->query($query);
            if (!$result) {
                error_log('Error ' . ' Mysql-raw error ' . json_encode($parameters));
                return false;
            }
            
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                $rows[] = $row;
            }
            
            return $rows;
        } catch(PDOException $e) {
            error_log('ERROR' . ' Mysql->raw error '  . json_encode($parameters) . ' ' . $e->getMessage() . "\n");
            return false;
        }
   
    }
    
    /**
    * Update a Row
    *
    * Update a row in database
    *
    * @since 0.0.0
    * @access public
    *
    * @param array $parameter ['table' => table, 'fields' => [], 'id'=>1]
    * @return object of the row
    * @return NULL number of records failed    
    * @return false if table doesn't exist
    */    
    public function save(array $parameters, $slug=false) {
        try {
            $table = $parameters['table'];
            $id = $parameters['id'];
            $identifier = ($slug)?'slug':'id';
            if ($identifier == 'slug') {
                $id = '"' . $id . '"';
            }            
            $fields = $this->processWhere($parameters['fields'], 
                ',', function($key, $value){
                    if (is_numeric($value)){
                        return "$key=$value";
                    } else if (is_bool($value)) {
                        $val = ($value == true)?'true':'false';
                        return "$key='$val'"; 
                    } else {
                        return "$key='" . $value . "'";
                    }            
            });
            
            $query = "UPDATE $table SET $fields WHERE $identifier = $id";
            // echo $query;
            //error_log('INFO ' . $query);
            $result = $this->_db->query($query);

            if (!$result) {
                error_log('Error ' . ' Mysql-save error ' . json_encode($parameters));
                return false;
            }

            return $result;
            
        } catch(PDOException $e) {
            error_log('ERROR' . ' Mysql->save error'   . json_encode($parameters) . ' ' . $e->getMessage() . "\n");
            return false;
        }                
    }
    
    /**
    * Add Row to database
    *
    * add Row to database tablet
    *
    * @since 0.0.0
    * @access public
    *
    * @param array $parameter ['table' => table, 'fields' => []]
    * @return integer row ID  
    * @return false if table doesn't exist
    */    
    public function add(array $parameters) {
        try {
            $table = $parameters['table'];
            $fields = $parameters['fields'];
            $rows = [];
            
            $fieldName = implode(',', array_keys($fields));
            $fieldValues = implode(',', $this->escape(array_values($fields)));
            
            $query = "INSERT INTO $table ($fieldName) VALUES ($fieldValues)";
            // echo $query;
            //error_log('INFO ' . $query);
            
            $result = $this->_db->query($query);
            
            if (!$result) {
                error_log('Error ' . ' Mysql-add table does not exist ' . json_encode($parameters));
                return false;
            }

            return $this->_db->lastInsertId();
            
        } catch(PDOException $e) {
            error_log('ERROR' . ' Mysql->add error ' . json_encode($parameters) . ' ' . $e->getMessage() . "\n");
            return false;
        }    
    }
    
    /**
    * Delete Row from Database
    *
    * Delete a row given id
    *
    * @since 0.0.0
    * @access public
    *
    * @param array $parameter ['table' => table, 'id' => []]
    * @param boolean $slug if slug true then select by that field
    * @return object of result
    * @return false if table doesn't exist
    */    
    public function delete(array $parameters, $slug=false) {
        try {
            $table = $parameters['table'];
            $id = $parameters['id'];
            $identifier = ($slug)?'slug':'id';
            if ($identifier == 'slug') {
                $id = '"' . $id . '"';
            }            
            $query = "DELETE FROM $table WHERE $identifier = $id";
            // echo $query;
            //error_log('INFO ' . $query);
            $result = $this->_db->query($query);

            if (!$result) {
                error_log('Error ' . ' Mysql-delete error ' . json_encode($parameters));
                return false;
            }

            return $result;
            
        } catch(PDOException $e) {
            error_log('ERROR' . ' Mysql->delete error'   . json_encode($parameters) . ' ' . $e->getMessage() . "\n");
            return false;
        }            
    }
    
    private function processWhere($where, $delimiter, $func) {
        $str = '';
        $rows = [];
        
        foreach ($where as $key => $value) {
            $rows[] = call_user_func_array($func, [$key, $value]);    
        }
        
        if (count($rows)  > 0) {
            $str = implode($delimiter, $rows);
        } else {
            $str = ' 1 '; 
        }
        
        return $str;
    }
    
    private function escape($list) {
        foreach ($list as $key => $value) {
            if (is_numeric($value)){
                $list[$key] = "$value";
            } else if (is_bool($value)) {
                $list[$key] = ($list[$key] == true)?'true':'false'; 
            } else {
                $list[$key] = "'" . $list[$key] . "'";
            }
        }
        return $list;
    }
}