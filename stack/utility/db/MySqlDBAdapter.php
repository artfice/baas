<?php 
class MySqlDBAdapter {
    private $_host;
    private $_user;
    private $_password;
    private $_dbName;
    private $_port;
    private $_encoding;
    public function __construct($config) {
        $hasDbParameter = !empty($config['db_host']) ||
                          !empty($config['db_user']) ||
                          !empty($config['db_password']) ||
                          !empty($config['db_dbName']) ||
                          !empty($config['db_port']) ||
                          !empty($config['db_encoding']);
        if (!$hasDbParameter){
            throw new Exception('Database Config missing');
        }
        $this->_host = $config['db_host'];
        $this->_user = $config['db_user'];
        $this->_password = $config['db_password'];
        $this->_dbName = $config['db_dbName'];
        $this->_port = $config['db_port'];
        $this->_encoding = $config['db_encoding'];
    }
    
    public function getHost() {
        return $this->_host;
    }
    
    public function getUser() {
        return $this->_user;
    }
    
    public function getPassword() {
        return $this->_password;
    }
    
    public function getDbName() {
        return $this->_dbName;
    }            
    
    public function getPort() {
        return $this->_port;
    }
    
    public function getEncoding() {
        return $this->_encoding;
    }    
        
    public function getInstance() {
        $dsn = "mysql:dbname=" . $this->_dbName . ";host=" . $this->_host . ";port=" . $this->_port;
        return new PDO($dsn, $this->_user, $this->_password);
    }
}