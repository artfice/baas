<?php 

class APIResponses {
    //401 Unauthorized
    //402 payment required
    //403 forbidden legal request, server refuse to respond
    //404 not found
    //405 method not allowed - using body in get
    //406 not acceptable - due to accept header, cannot generate that type of content
    //408 timeout response
    //409 conflict edit data conflict
    //410 Gone api gone forever
    //411 Length required - content need length specified
    //412 Precondition failed 
    //413 Payload too large
    //414 Url Too long
    //415 unsupported media type
    const VALID = 1000;
    const REQUIREERROR = 1001;
    const ERRORNOTFOUND = 1002;
    const TYPEERROR = 1003;
    const ENUMERROR = 1004;
    const INVALIDSWAGGERERROR = 1005;
    const POLICYERROR = 1006;
    const CLIENTNOTFOUND = 1007; 
    
    const DATA = [
        1000 => [
            'message' => '',
            'status'  => 'SUCCESS',
            'code'    => 200            
        ],
        1001 => [
            'message' => 'Required Field Missing',
            'status'  => 'ERROR',
            'code'    => 412
        ],
        1002 => [
            'message' => 'Server Error',
            'status'  => 'ERROR',
            'code'    => 500            
        ],
        1003 => [
            'message' => 'Field Type Mismatch',
            'status'  => 'VALIDATION_ERROR',
            'code'    => 412            
        ],
        1004 => [
            'message' => 'Field Value Not in Valid List',
            'status'  => 'VALIDATION_ERROR',
            'code'    => 412            
        ],
        1005 => [
            'message' => 'Swagger Invalid',
            'status'  => 'SERVER_ERROR',
            'code'    => 403         
        ],
        1006 => [
            'message' => 'Policy Error',
            'status'  => 'POLICY_ERROR',
            'code'    => 405         
        ],
        1007 => [
            'message' => 'Client ID not found',
            'status'  => 'VALIDATION_ERROR',
            'code'    => 412            
        ],                
    ];
    public static function send ($code, $returnString=false) {
        if (array_key_exists($code, APIResponses::DATA)) {
            if ($code === APIResponses::VALID) {
                return true;
            }
            if ($returnString) {
                return [
                    'code' => APIResponses::DATA[$code], 
                    'payload' => APIResponses::DATA[$code]['code']
                ];
            } else {
                Flight::json(APIResponses::DATA[$code], APIResponses::DATA[$code]['code']);
                exit;                
            }
        } else {
            Flight::json(APIResponses::DATA[APIResponses::ERRORNOTFOUND], APIResponses::DATA[APIResponses::ERRORNOTFOUND]['code']);
            exit;
        }
    }
}