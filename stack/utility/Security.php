<?php
class Security {
    /**
     * 
     * @param string $password
     * @param string $salt
     * @return string
     */
    public static function hashPasswordForDatabase($password, $salt) {
        $plainText = $salt . $password . $salt;
        $hash1 = sha1($plainText);
        $hash2 = sha1($hash1);
        return $hash2;
    }
    /**
     * 
     * @param string $password
     * @param int $size
     * @return string
     */
    public static function hashPassword($password, $key, $size = 8) {
        $plainText = Security::HASHSALT . $password . $key;
        $hash = sha1($plainText);
        return substr($hash, 0, $size);
    }
    /**
     * 
     * @param string $text
     * @return string
     */
    public static function encryptDES($text) {
        $key = 'qFS8LRE6XGZmNx9idHFK6AYC';
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_3DES, $key, $text, MCRYPT_MODE_ECB));
        return $encrypted;
    }
    /**
     * 
     * @param string $cipher
     * @return string
     */
    public static function decryptDES($cipher) {
        $key = 'qFS8LRE6XGZmNx9idHFK6AYC';
        $decrypted = mcrypt_decrypt(MCRYPT_3DES, $key, base64_decode($cipher), MCRYPT_MODE_ECB);
        return $decrypted;
    }
    /**
     * 
     * @param int $length
     * @param boolean $puncuation
     * @return string
     */
    public static function randString($length, $puncuation = false) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        if ($puncuation) {
            $chars .= "`~!@#$%^&*()-=+\|[]{};:";
        }
        $str = '';
        $size = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }
    
    /**
    * @author Tom Worster <fsb@thefsb.org>
    * @copyright Copyright (c) 2008 Yii Software LLC
    * @license 3-Clause BSD. See XTRAS.md in this Gist.
    */
    /**
    * Generates specified number of random bytes. Output is binary string, not ASCII.
    *
    * @param integer $length the number of bytes to generate
    *
    * @return string the generated random bytes
    * @throws \Exception
    */
    public static function randomBytes($length) {
        if (function_exists('random_bytes')) {
            return random_bytes($length);
        }
        if (!is_int($length) || $length < 1) {
            throw new \Exception('Invalid first parameter ($length)');
        }
        // The recent LibreSSL RNGs are faster and better than /dev/urandom.
        // Parse OPENSSL_VERSION_TEXT because OPENSSL_VERSION_NUMBER is no use for LibreSSL.
        // https://bugs.php.net/bug.php?id=71143
        if (defined('OPENSSL_VERSION_TEXT')
            && preg_match('{^LibreSSL (\d\d?)\.(\d\d?)\.(\d\d?)$}', OPENSSL_VERSION_TEXT, $matches)
            && (10000 * $matches[1]) + (100 * $matches[2]) + $matches[3] >= 20105
        ) {
            $key = openssl_random_pseudo_bytes($length, $cryptoStrong);
            if ($cryptoStrong === false) {
                throw new \Exception('openssl_random_pseudo_bytes() set $crypto_strong false. Your PHP setup is insecure.');
            }
            if ($key !== false && mb_strlen($key, '8bit') === $length) {
                return $key;
            }
        }
        // mcrypt_create_iv() does not use libmcrypt. Since PHP 5.3.7 it directly reads
        // CrypGenRandom on Windows. Elsewhere it directly reads /dev/urandom.
        if (PHP_VERSION_ID >= 50307 && function_exists('mcrypt_create_iv')) {
            $key = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
            if (mb_strlen($key, '8bit') === $length) {
                return $key;
            }
        }
        // If not on Windows, test for a /dev/urandom device.
        if (DIRECTORY_SEPARATOR === '/') {
            // Check it for speacial character device protection mode. Do not follow
            // symbolic link at '/dev/urandom', as such would be suspicious. With lstat()
            // (as opposed to stat()) the test fails if it is.
            $lstat = @lstat('/dev/urandom');
            if ($lstat !== false && ($lstat['mode'] & 0170000) === 020000) {
                $key = @file_get_contents('/dev/urandom', false, null, 0, $length);
                if ($key !== false && mb_strlen($key, '8bit') === $length) {
                    return $key;
                }
            }
        }
        // Since 5.4.0, openssl_random_pseudo_bytes() reads from CryptGenRandom on Windows instead
        // of using OpenSSL library. Don't use OpenSSL on other platforms.
        if (DIRECTORY_SEPARATOR !== '/' && PHP_VERSION_ID >= 50400 && defined('OPENSSL_VERSION_TEXT')) {
            $key = openssl_random_pseudo_bytes($length, $cryptoStrong);
            if ($cryptoStrong === false) {
                throw new \Exception('openssl_random_pseudo_bytes() set $crypto_strong false. Your PHP setup is insecure.');
            }
            if ($key !== false && mb_strlen($key, '8bit') === $length) {
                return $key;
            }
        }
        throw new \Exception('Unable to generate a random key');
    }
    
    
    /**
    * Generates a random UUID using the secure RNG.
    *
    * Returns Version 4 UUID format: xxxxxxxx-xxxx-4xxx-Yxxx-xxxxxxxxxxxx where x is
    * any random hex digit and Y is a random choice from 8, 9, a, or b.
    *
    * @return string the UUID
    */
    public static function randomUuid() {
        $bytes = randomBytes(16);
        $bytes[6] = chr((ord($bytes[6]) & 0x0f) | 0x40);
        $bytes[8] = chr((ord($bytes[8]) & 0x3f) | 0x80);
        $id = str_split(bin2hex($bytes), 4);
        return "{$id[0]}{$id[1]}-{$id[2]}-{$id[3]}-{$id[4]}-{$id[5]}{$id[6]}{$id[7]}";
    }
    
    /**
    * Returns a random integer in the range $min through $max inclusive.
    *
    * @param int $min Minimum value of the returned integer.
    * @param int $max Maximum value of the returned integer.
    *
    * @return int The generated random integer.
    * @throws \Exception
    */
    public static function randomInt($min, $max)
    {
        if (function_exists('random_int')) {
            return random_int($min, $max);
        }
        if (!is_int($min)) {
            throw new \Exception('First parameter ($min) must be an integer');
        }
        if (!is_int($max)) {
            throw new \Exception('Second parameter ($max) must be an integer');
        }
        if ($min > $max) {
            throw new \Exception('First parameter ($min) must be no greater than second parameter ($max)');
        }
        if ($min === $max) {
            return $min;
        }
        // $range is a PHP float if the expression exceeds PHP_INT_MAX.
        $range = $max - $min + 1;
        if (is_float($range)) {
            $mask = null;
        } else {
            // Make a bit mask of (the next highest power of 2 >= $range) minus one.
            $mask = 1;
            $shift = $range;
            while ($shift > 1) {
                $shift >>= 1;
                $mask = ($mask << 1) | 1;
            }
        }
        $tries = 0;
        do {
            $bytes = randomBytes(PHP_INT_SIZE);
            // Convert byte string to a signed int by shifting each byte in.
            $value = 0;
            for ($pos = 0; $pos < PHP_INT_SIZE; $pos += 1) {
                $value = ($value << 8) | ord($bytes[$pos]);
            }
            if ($mask === null) {
                // Use all bits in $bytes and check $value against $min and $max instead of $range.
                if ($value >= $min && $value <= $max) {
                    return $value;
                }
            } else {
                // Use only enough bits from $bytes to cover the $range.
                $value &= $mask;
                if ($value < $range) {
                    return $value + $min;
                }
            }
            $tries += 1;
        } while ($tries < 123);
        // Worst case: this is as likely as 123 heads in as many coin tosses.
        throw new \Exception('Unable to generate random int after 123 tries');
    }    
}


