<?php

class Oauth {
    const AUTH_TYPE_URI = 0; //client_id, client_secret
    const AUTH_TYPE_AUTHORIZATION_BASIC = 1; //client_id, base64(clientId:client_secret)
    
    const ACCESS_TOKEN_URI      = 0;
    const ACCESS_TOKEN_BEARER   = 1; //Bearer token
    const ACCESS_TOKEN_OAUTH    = 2;//OAuth token
    const ACCESS_TOKEN_MAC      = 3;//Mac generateMacSigniture

    const GRANT_TYPE_AUTH_CODE          = 'authorization_code';
    const GRANT_TYPE_PASSWORD           = 'password';
    const GRANT_TYPE_CLIENT_CREDENTIALS = 'client_credentials';
    const GRANT_TYPE_REFRESH_TOKEN      = 'refresh_token';

    protected $_clientId;
    protected $_clientSecret;
    protected $_accessToken;
    protected $_accessTokenType;
    protected $_accessTokenParamName;
    
    public function __construct() {
        //clientid, secret, , grant type
    }
    
    public function authEndpoint() {
        $basePath = "api.com";
        
        return "$basePath/response_type=code&client_id=client&redirect_uri=blah";
    }
}