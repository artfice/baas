<?php

class BasicAuth {
    private $_realm;
    private $_nonce;
    private $_digest;
    private $_users = [];
    public function __construct($config, $namespace) {
        $this->_nonce = uniqid();
        $this->_users[$config[$namespace . '_login']] = $config[$namespace . '_password'];
        $this->_realm = $config['realm']; 
    } 
    
    public function valid($user, $pass) {
        if (isset($this->_users[$user]) && ($this->_users[$user] == $pass)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function authenticate() {
        $user = !empty($_SERVER['PHP_AUTH_USER'])?$_SERVER['PHP_AUTH_USER']:''; 
        $pass = !empty($_SERVER['PHP_AUTH_PW'])?$_SERVER['PHP_AUTH_PW']:'';
        if (!$this->valid($user, $pass)){
            header('WWW-Authenticate: Basic realm="' . $this->_realm . '"');
            header('HTTP/1.0 401 Unauthorized');
            echo json_encode([
                "message" => "Invalid Credentials"
            ]);
            exit;            
        }
    }
}