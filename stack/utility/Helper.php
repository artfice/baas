<?php

class Helper {
    /**
     * Converts the class name to camel case
     *
     * @param  mixed  $grant_type  the grant type
     * @return string
     */
    public static function convertToCamelCase ($grant_type) {
        $parts = explode('_', $grant_type);
        array_walk($parts, function(&$item) { $item = ucfirst($item);});
        return implode('', $parts);
    }    
    
    public static function printr($data) {
        echo "<pre>" . print_r($data, true) . "</pre>";
    }
    
    public static function getInput($path = array()) {
        $query = Flight::request()->query->getData();
        $data = Flight::request()->data->getData();
        $paths = ['validation_path' => $path];
        $authorization = [
                'authorization' => Flight::request()->authorization
            ];
            
        $result = array_merge($authorization, $data, $paths, $query);
        
        //if authorization in GET query, replace the one in header
        $authorization = $result['authorization'];
        unset($result['authorization']);
        
        return [
            'authorization' => $authorization,
            'body' => $result
        ];
    }
}